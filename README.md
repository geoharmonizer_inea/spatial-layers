# Geo-harmonizer spatial layers

This repository includes all production steps used to produce spatial layers 
at various spatial resolutions (from 30 m to 1 km). For more info about the 
technical specifications also refer to the https://opendatascience.eu/geoharmonizer-project website.


# File naming convention
Within the Geo-harmonizer project we consistently use a file naming convention in the format e.g.:

- `dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.1.tif`

with the following fields:

- theme: e.g. `dtm`,
- variable code: e.g. `elev.lowestmode`,
- variable estimation method: e.g. `gedi.eml`,
- variable estimation type: e.g. `m`,
- spatial support (usually horizontal block) in m e.g.: `30m`,
- reference depths (vertical dimension): e.g. `0..0cm`,
- reference period begin end: e.g. `2000..2018`,
- reference area: `eumap`,
- coordinate system: e.g. `epsg3035`,
- data set version: e.g. `v0.1`,

The variable code should be used consistently in all geocomputing. Variable estimation method 
should be directly mentioned in the metadata with a reference to literature or URL where one 
can find complete documentation. 

Note from the example above the following:

-   `-` is used to indicate a field,
-   no mathematical symbols are used e.g. "+" or "/",
-   no capitalized letters are used,
-   `..` is used to indicate range,
-   `.` is used to indicate space,

Variable estimation type can be one of the following (see also the [OpenLandMap.org projecty](https://gitlab.com/openlandmap/global-layers/)):

    1.  `m` = mean value,
    2.  `d` = median value,
    3.  `l.159` = lower 68% probability threshold (quantile),
    4.  `u.841` = upper 68% probability threshold (quantile), for one-sided probability use `uo`,
    5.  `sd.1` = 1 standard deviation (`sd` can be also used, sd_p in case of propagated error between two years),
    6.  `md` = model deviation (in the case of ensemble predictions),
    7.  `td` = cumulative difference (usually based on time-series of values),
    8.  `c` = classes i.e. factor variable,
    9.  `p` = probability or fraction,
    10.  `sse` = Shannon Scaled Entropy index,
    11.  `q` = quality index or indicator,
    12. `cc` = cumulative cost (between locations on a raster whose values represent cost)

For temporal reference period please use the date / time format that starts with Year:

```
> format(Sys.Date(), "%Y.%m.%d")
[1] "2020.09.27"
```

If not specified otherwise, all layers contributed in the Geo-harmonizer project are 
prepared as [Cloud Optimized GeoTIFFs](https://www.cogeo.org/) 
(see metadata `-co "CO=TRUE"`) in the case of gridded data, and/or 
GeoPackage and/or [GeoPackage](https://gdal.org/drivers/vector/gpkg.html) and/or [FlatGeobuf](https://gdal.org/drivers/vector/flatgeobuf.html) files in the case of 
vector data (see [technical specifications](https://opendatascience.eu/geo-harmonizer-implementation-plan-2020-2022#Data_storage_model)).


# The land mask

The `eumap` bounding box of interest is:

```
Xmin =   900,000
Ymin =   930,010
Xmax = 6,540,000
Ymax = 5,460,010
```

This is based on the [EPSG:3035](https://epsg.io/3035) reference system with the following 
proj4 parameters:

```
+init='epsg:3035'
```

The image sizes for that bounding box at various standard resolutions are:

-   30m = 188000P x 151000L,
-   100m = 56400P x 45600L,
-   250m = 22560P x 18240L,
-   1km = 56400P x 4560L,

![eumap land mask](assets/img/eumap_landmask_preview_750px.jpg)

*Image: Area of interest (continental EU) includes EEA, without the Caribbean and Indian Ocean islands.*


# Standard tiling system
Recommended tiling system is:

-  `adm_tiling.system_30km_c_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg`

```
eumap_epsg3035_v0.1.gpkg", layer: "tiles_landmask_30km"
with 7042 features
It has 16 fields
```

This is the 30 by 30 km tiling system. Each tile has an unique ID (see `fid`) which 
starts from the bottom left. Other attributes of the tiles include:

```
> str(tiles@data)
'data.frame':	7042 obs. of  16 variables:
 $ xl           : num  1770000 1800000 1890000 1920000 1950000 1770000 1800000 1920000 1950000 1980000 ...
 $ yl           : num  930010 930010 930010 930010 930010 ...
 $ xu           : num  1800000 1830000 1920000 1950000 1980000 1800000 1830000 1950000 1980000 2010000 ...
 $ yu           : num  960010 960010 960010 960010 960010 ...
 $ offst_y      : num  150000 150000 150000 150000 150000 149000 149000 149000 149000 149000 ...
 $ offst_x      : num  29000 30000 33000 34000 35000 29000 30000 34000 35000 36000 ...
 $ rgn_dm_y     : num  1000 1000 1000 1000 1000 1000 1000 1000 1000 1000 ...
 $ rgn_dm_x     : num  1000 1000 1000 1000 1000 1000 1000 1000 1000 1000 ...
```

Where `xl`, `yl`, `xu` and `yu` are the lower left and upper right coordinates of the tile, 
`offst_y` and `offst_x` are the offsets in the image coordinates (starting from top left), and 
`rgn_dm_y` and `rgn_dm_x` are the tile dimensions in image coordinates.

# List of datasets available
Available layers for continental Europe at 30m:

- Land mask,
- Digital Terrain Model (slope and elevation),
- Quarterly GLAD Landsat composites (all bands + NDVI, 2000–2020),
- Quarterly Sentinel-2 L2A composites (all bands, 2000–2020),
- Annual dominant land cover class based on CORINE (2000–2019),
- Annual probabilities and uncertainties for 33 CORINE land cover classes (2000–2019),
- Density of building areas according to OSM (2021) and Copernicus (impervious built-up - 2018),
- Density of commercial, industrial and residential buildings according to OSM (2021),
- Harmonized protected areas derived from OSM (2021) and NATURA2000 (2019),
- Harmonized administrative areas (county-level) derived from OSM (2021) and NUTS (2021),
- Probabilities of 18 OSM land cover classes (2021),
- Density of highways and railways according to OSM (2021).
- Daily minimum, average and maximum surface temperature based on ERA5 (2000–2020).
- Daily minimum, average and maximum air temperature based on ERA5 (2000–2020).
- Daily sum of precipitation based on ERA5 (2000-2020).
- Seasonal Copernicus EMS flood and fire activations (2012–2020).
- Seasonal MODIS burned areas (2000–2020).
- Potential and realized distribution (probability maps) for 16 forest tree species, aggregated by 4 years time window (2000-2020).
- Monthly air quality maps (PM2.5) based on MODIS.
- Soil properties (pH, organic carbon, bulk density, clay content and sand content) at four depths aggregated by 4 years time window (2000-2020). 

## SpatioTemporal Asset Catalog

**[http://stac.opendatascience.eu](http://stac.opendatascience.eu)**

![ODSE - SpatioTemporal Asset Catalog](assets/img/stac.png)

## Metadata Catalog

**[https://data.opendatascience.eu/geonetwork](https://data.opendatascience.eu/geonetwork)**

![eumap land mask](assets/img/geonetwork.png)

## Data Viewer

**[http://maps.opendatascience.eu](http://maps.opendatascience.eu)**

![eumap land mask](assets/img/viewer.png)