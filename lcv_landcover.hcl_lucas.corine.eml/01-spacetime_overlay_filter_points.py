import argparse
import joblib
import sys

from eumap.mapper import SpaceOverlay, SpaceTimeOverlay
from eumap.misc import ttprint, find_files

import os

from pathlib import Path

import sys
from datetime import datetime
import glob
import joblib
from pyproj import Proj, transform
import pandas as pd
import geopandas as gpd

import numexpr as ne
from typing import List
import multiprocessing
from eumap.mapper import SpaceOverlay, SpaceTimeOverlay
from eumap.misc import ttprint, find_files
import math
from pathlib import Path
import numpy as np 

def load_pts(path_pts,epsg=3035,print_uniques=None):
    assert os.path.exists(path_pts)
    if path_pts.endswith(".joblib"):
        pts = joblib.load(path_pts)
    if path_pts.endswith(".csv"):
        pts = dt.fread(path_pts).to_pandas()
        pts = gpd.GeoDataFrame(pts, geometry=gpd.points_from_xy(pts.x,pts.y))
        pts = pts.set_crs(f"EPSG:{epsg}")
    if path_pts.endswith(".gpkg"):
        pts = gpd.read_file(path_pts)
    if path_pts.endswith(".shp"):
        pts = gpd.read_file(path_pts)
        pts['x'] = pts.geometry.x
        pts['y'] = pts.geometry.y
        print(pts.x)
        
    if print_uniques is not None:
        unique_counts = pts[print_uniques].value_counts()
        ttprint(len(unique_counts),'\n',unique_counts)
        
    return(pts)

def add_tile_ids(pts,path_tiles):
    tiles = gpd.read_file(path_tiles)
    overlay = gpd.overlay(pts, tiles, how='intersection')
    return(overlay)

def layerUrl(layername,url='http://192.168.1.57:9000',bucket='eumap'):
    prefix = layername.split('_')[0]
    url = str(Path(f"{url}") \
            .joinpath(bucket) \
            .joinpath(prefix) \
            .joinpath(layername))
    return url

def remove_bad_dates(pts,date_col):
    # Drop rows with invalid dates that cannot be converted to proper datetime objects
    u = pd.unique(pts[date_col])
    for i in u:
        try:
            pd.to_datetime(i)
        except:
            old = len(pts)
            pts = pts[pts[date_col] != i]
            new = len(pts)
            ttprint(f"Removed {old-new} rows with bad date {i}")
    return(pts)

def filter_overlay(pts, osm_layers=[
                       'lcv_railway_osm_p_30m_0..0cm_2021_eumap_epsg3035_v0.1',
                       'lcv_road_osm_p_30m_0..0cm_2021_eumap_epsg3035_v0.1',
                       'lcv_building_copernicus.osm_p_30m_0..0cm_2018..2021_eumap_epsg3035_v0.1'], 
                   dir_cop='/mnt/diskstation/eu_data/OSM+/Copernicus/',verbose=False):
    fn_layers = [Path(layerUrl(i+'.tif')) for i in osm_layers]
    fn_layers += [Path(i) for i in glob.glob(dir_cop + "/*2018*.tif")]
    space_overlay = SpaceOverlay(points=pts, fn_layers=fn_layers, verbose=verbose)
    pts = space_overlay.run()
    return(pts)

def remove_na_points(pts,first_covariate_idx,last_covariate_idx,target_col):
    # Drop rows with NA values in the target column or the covariates
    l_total = len(pts)
    pts = pts.dropna(subset=pts.columns[first_covariate_idx:last_covariate_idx])
    cov_removed = l_total - len(pts)
    ttprint(f"Removed {cov_removed} points due to covariate NA values")
    pts = pts.dropna(subset=[target_col])
    tgt_removed = l_total - cov_removed - len(pts)
    ttprint(f"Removed {tgt_removed} points due to target variable ({target_col}) NA values")
    return(pts)

def filter_class(df, class_val, rule ,method='keep'):
    
    
    mask = np.logical_and.reduce((df['clc3_orig'].isin([class_val]), df['lucas'] == 'f'))
    total_points = df[mask].shape[0]
    if method == 'keep':
        mask = np.logical_and(mask, np.logical_not(rule))
    if method == 'drop':
        mask = np.logical_and(mask, rule)
        
    rm_points = df[mask].shape[0]
    perc = (rm_points)/total_points*100 if rm_points > 0 else 0
    print(f"\t{class_val}: {total_points:6} - {rm_points:6} = {total_points-rm_points:6} {perc:3.1f}% ")
    return [df.drop(df[mask].index),rm_points]

def filter_points(pts):
    
    osm_railways = 'lcv_railway_osm_p_30m_0..0cm_2021_eumap_epsg3035_v0.1'
    osm_roads = 'lcv_road_osm_p_30m_0..0cm_2021_eumap_epsg3035_v0.1'
    osm_cop_buildings = 'lcv_building_copernicus.osm_p_30m_0..0cm_2018..2021_eumap_epsg3035_v0.1'

    cop_tree_cover = 'TCD_2018_010m_eu_03035_V2_0'
    cop_impervious = 'IMD_2018_010m_eu_03035_V2_0'
    cop_grasslands = 'GRA_2018_010m_eu_03035_V1_0'
    cop_wet_perm = 'WAW_2018_010m_eu_03035_V2_0_3'
    cop_wet_temp = 'WAW_2018_010m_eu_03035_V2_0_4'
    cop_water_perm = 'WAW_2018_010m_eu_03035_V2_0_1'
    
        
    lc_codes = [111, 112, 121, 122, 123, 124, 131, 132, 133, 141, 142, 
                211, 212, 213, 221, 222, 223, 231, 241, 242, 243, 244, 
                311, 312, 313, 321, 322, 323, 324, 331, 332, 333, 334, 335, 
                411, 412, 421, 422, 423, 
                511, 512, 521, 522, 523]
    
    r = pd.DataFrame(lc_codes)
    r.columns = ['clc3']
    r['original'] = [pts.clc3_orig.value_counts()[i] for i in lc_codes]
    
    ttprint('Keep urban with buildings inside 50-150')
    r['urban'] = 0
    pts, r['urban'][r.clc3 == 111] = filter_class(pts, 111, np.logical_and.reduce((pts[osm_cop_buildings] > 50, pts[osm_cop_buildings] < 150)))
    pts, r['urban'][r.clc3 == 112] = filter_class(pts, 112, np.logical_and.reduce((pts[osm_cop_buildings] > 50, pts[osm_cop_buildings] < 150)))

    ttprint('Keep non-urban with buildings outside 50-150')
    r['non-urban'] = 0
    pts, r['non-urban'][r.clc3 == 141] = filter_class(pts, 141, np.logical_and.reduce((pts[osm_cop_buildings] > 50, pts[osm_cop_buildings] < 150)),method='drop')
    for lc_code in lc_codes[11:]:
        pts, r['non-urban'][r.clc3 == lc_code] = filter_class(pts, lc_code, np.logical_and.reduce((pts[osm_cop_buildings] > 50, pts[osm_cop_buildings] < 150)),method='drop')

    ttprint('Keep roads & rails with rails/roads OR impervious >30')
    r['roads_rails_imp'] = 0
    pts, r['roads_rails_imp'][r.clc3 == 122] = filter_class(pts, 122, np.logical_or.reduce((
        pts[osm_railways] > 30,
        pts[osm_roads] > 30,
        pts[cop_impervious] > 30)))

    ttprint('Keep 200+ classes with rails/roads < 30')
    r['no_roads_rails'] = 0
    for lc_code in lc_codes[11:]:
        pts, r['no_roads_rails'][r.clc3 == lc_code] = filter_class(pts,lc_code, np.logical_and.reduce((
            np.logical_or.reduce((pts[osm_railways] < 30, pts[osm_railways].isnull())),
            np.logical_or.reduce((pts[osm_roads] < 30, pts[osm_roads].isnull())))))

    ttprint('Keep 211,212,231,321 without tree cover')
    r['no_trees'] = 0
    for lc_code in [211,212,231,321]:
        pts, r['no_trees'][r.clc3 == lc_code] = filter_class(pts,lc_code, pts[cop_tree_cover].isnull())

    ttprint('Keep 141 with either tree cover or grass cover')
    pts, r['no_trees'][r.clc3 == 141] = filter_class(pts,141,np.logical_or.reduce((pts[cop_tree_cover] > 0, pts[cop_grasslands] >0)))

    ttprint('Keep 221, 222, 223 without grass cover')
    r['no_grass'] = 0
    for lc_code in [221,222,223]:
        pts, r['no_grass'][r.clc3 == lc_code] = filter_class(pts, lc_code, pts[cop_grasslands].isnull())

    ttprint('Keep 244,311,312,313 with tree cover')
    r['trees'] = 0
    for lc_code in [244,311,312,313]:
        pts, r['trees'][r.clc3 == lc_code] = filter_class(pts, lc_code, pts[cop_tree_cover] > 25)

    ttprint('Keep 400 classes with perm or temp wetness above 0')
    r['wet'] = 0
    for lc_code in [411,412,421,422,423]:
        pts, r['wet'][r.clc3 == lc_code] = filter_class(pts, lc_code, np.logical_or.reduce((pts[cop_wet_perm] > 0, pts[cop_wet_temp] > 0)))

    ttprint('Keep 321 with grass cover above 0')
    r['grass'] = 0
    pts, r['grass'][r.clc3 == 321] = filter_class(pts, 321, pts[cop_grasslands] > 0)

    ttprint('Keep 511,521,522 with permanent water above 50')
    r['water_50'] = 0
    for lc_code in [511,521,522]:
        pts, r['water_50'][r.clc3 == lc_code] = filter_class(pts, lc_code, pts[cop_water_perm] > 50)

    ttprint('Keep 512, 523 with permanent water 100')
    r['water_100'] = 0
    for lc_code in [512,523]:
        pts, r['water_100'][r.clc3 == lc_code] = filter_class(pts, lc_code, pts[cop_water_perm] == 100)
    
    r['removed']= r.iloc[:, 2:].sum(axis=1)
    r['remaining'] = r.original - r.removed
    r['removed_pct'] = r.removed / r.original * 100
    
    r.to_latex("filter_report.tex",index=False,float_format="%.3f")
    return([pts, r])



def overlay(pts,col_date,ls_version='1.0',dtm_version='0.2',hyd_version='0.1',verbose=True,check_exists=True,dir_minio='/mnt/gaia/minio'):
    
    if ls_version == '1.0':
        seasons = ['{year_minus_1}1202..{year}0320','{year}0321..{year}0624','{year}0625..{year}0912','{year}0913..{year}1201']
    elif ls_version == '1.1':
        seasons = ['{year_minus_1}.12.02..{year}.03.20','{year}.03.21..{year}.06.24','{year}.06.25..{year}.09.12','{year}.09.13..{year}.12.01']
    
    timeless_layers = [
        Path(layerUrl(f'hyd_surface.water_jrc.gswe_p_30m_0..0cm_1984..2019_eumap_epsg3035_v{hyd_version}.tif')),
        Path(layerUrl(f'dtm_slope.percent_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_hillshade.multid_gedi.gdal_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_hillshade.a315_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_openp_gedi.saga.gis_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_openn_gedi.saga.gis_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_northness_gedi.grass7_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_easterness_gedi.grass7_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif')),
        Path(layerUrl(f'dtm_cost.distance.to.coast_gedi.grass.gis_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}.tif'))
    ]
    
    st_layers = []
    for band in ['blue', 'green', 'red', 'nir', 'swir1', 'swir2', 'thermal']:
        for perc in ['p25', 'p50', 'p75']:
            st_layers.append( Path(layerUrl(f'lcv_{band}_landsat.glad.ard_{perc}_30m_0..0cm_'+'{year_minus_1}.12.02..{year}.03.20_eumap_epsg3035_v'+ls_version+'.tif')))
            st_layers.append( Path(layerUrl(f'lcv_{band}_landsat.glad.ard_{perc}_30m_0..0cm_'+'{year}.06.25..{year}.09.12_eumap_epsg3035_v'+ls_version+'.tif')))
            st_layers.append( Path(layerUrl(f'lcv_{band}_landsat.glad.ard_{perc}_30m_0..0cm_'+'{year}.03.21..{year}.06.24_eumap_epsg3035_v'+ls_version+'.tif')))
            st_layers.append( Path(layerUrl(f'lcv_{band}_landsat.glad.ard_{perc}_30m_0..0cm_'+'{year}.09.13..{year}.12.01_eumap_epsg3035_v'+ls_version+'.tif')))
    seasons_nighttime = ['{year_minus_1}.12.01..{year}.02.28',
                         '{year}.03.01..{year}.05.31',
                         '{year}.06.01..{year}.08.31',
                         '{year}.09.01..{year}.11.30']


    for season in seasons_nighttime:
        st_layers.append(layerUrl('clm_lst_mod11a2.nighttime_u0.95_1km_s0..0cm_'+season+'_eumap_epsg3035_v1.1.tif')) # TODO: Rename to add rename to add 
    st_layers.append(layerUrl('lcv_night.light_suomi.npp.viirs_avg.rade9h_30m_0..0cm_{year}_eumap_epsg3035_v0.1.tif') )
    st_layers.append(layerUrl('lcv_green.tri_landsat.glad.ard_p50_30m_0..0cm_{year}0625..{year}0912_eumap_epsg3035_v1.0.tif')) #Todo: update version (1.1 does not exist) 
    
    spc_overlay = SpaceOverlay(pts,fn_layers=timeless_layers, verbose=verbose)
    pts = spc_overlay.run()

    spt_overlay = SpaceTimeOverlay(pts, col_date, st_layers, verbose=verbose)
    pts = spt_overlay.run()
    
    return(pts)

def add_latlon(pts):
    inProj = Proj(init='epsg:3857')
    outProj = Proj(init='epsg:4326')
    pts['longitude'], pts['latitude'] = transform(inProj,outProj,pts.x.to_numpy(),pts.y.to_numpy())
    return(pts)

def geo_temp(fi, day, a=37.03043, b=-15.43029):
    f =fi
    pi = math.pi 

    #math.cos((day - 18) * math.pi / 182.5 + math.pow(2, (1 - math.copysign(1, fi))) * math.pi) 
    sign = 'where(abs(fi) - fi == 0, 1, -1)'
    costeta = f"cos((day - 18) * pi / 182.5 + 2**(1 - {sign}) * pi)"

    #math.cos(fi * math.pi / 180)
    cosfi = "cos(fi * pi / 180)"
    A = cosfi

    #(1 - costeta) * abs(math.sin(fi * math.pi / 180) )
    B = f"(1 - {costeta}) * abs(sin(fi * pi / 180) )"

    x = f"a * {A} + b * {B}"
    return ne.evaluate(x)

def add_geo_temp(pts,dtm_version='0.2'):
    pts = add_latlon(pts)

    elev_corr = 0.006 * pts[f'dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v{dtm_version}'].to_numpy()

    for m in range(1,13):
        doy = (datetime.strptime(f'2000-{m}-15', '%Y-%m-%d').timetuple().tm_yday)
        max_temp_name = f'clm_lst_max.geom.temp_m_30m_s0..0cm_m{m}' 
        min_temp_name = f'clm_lst_min.geom.temp_m_30m_s0..0cm_m{m}'
        pts[max_temp_name] = geo_temp(pts.latitude.to_numpy(), day=doy, a=37.03043, b=-15.43029) - elev_corr
        pts[min_temp_name] = geo_temp(pts.latitude.to_numpy(), day=doy, a=24.16453, b=-15.71751) - elev_corr

    return(pts)

def add_indices(pts,ls_version='1.0'):
    
    def ndvi(b3, b4):
        data = ne.evaluate('(b4/255 - b3/255) / (b3/255 + b4/255)')
        data[~np.isfinite(data)] = 0
        return data

    #source: https://www.usgs.gov/core-science-systems/nli/landsat/landsat-enhanced-vegetation-index?qt-science_support_page_related_con=0#qt-science_support_page_related_con
    def evi(b4, b3, b1):
        data = ne.evaluate('2.5 * ((b4/255 - b3/255) / (b4/255 + 6 * b3/255 - 7.5/255 * b1 + 1))')
        data[~np.isfinite(data)] = 0
        return data

    #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-soil-adjusted-vegetation-index
    def savi(b4, b3):
        data = ne.evaluate('((b4/255 - b3/255) / (b4/255 + b3/255 + 0.5)) * (1.5)')
        data[~np.isfinite(data)] = 0
        return data

    #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-modified-soil-adjusted-vegetation-index
    def msavi(b4, b3):
        data = ne.evaluate('(2 * b4/255 + 1 - sqrt(((2 * b4/255 + 1)**2) - (8 * (b4/255 - b3/255)))) / 2')
        data[~np.isfinite(data)] = 0
        return data

    #https://www.usgs.gov/core-science-systems/nli/landsat/normalized-difference-moisture-index
    def ndmi(b4, b5):
        data = ne.evaluate('(b4/255 - b5/255) / (b4/255 + b5/255)')
        data[~np.isfinite(data)] = 0
        return data

    #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio
    def nbr(b4, b7):
        data = ne.evaluate('(b4/255 - b7/255) / (b4/255 + b7/255)')
        data[~np.isfinite(data)] = 0
        return data

    #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio-2
    def nbr2(b5, b7):
        data = ne.evaluate('(b5/255 - b7/255) / (b5/255 + b7/255)')
        data[~np.isfinite(data)] = 0
        return data

    def rei(b1, b5, b4):
        data = ne.evaluate('(b4/255 - b1/255) / (b5/255 + (b1/255 * b5/255))')
        data[~np.isfinite(data)] = 0
        return data

    def ndwi(b2, b5):
        data = ne.evaluate('((b2/255 - b5/255) / (b2/255 + b5/255))')
        data[~np.isfinite(data)] = 0
        return data

    if ls_version == '1.0':
        seasons = ['1202..0320','0321..0624','0625..0912','0913..1201']
    elif ls_version == '1.1':
        seasons = ['.12.02...03.20','.03.21...06.24','.06.25...09.12','.09.13...12.01']

    for perc in ['25','50','75']:
        for season in seasons:
            b1 = pts[f'lcv_blue_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b2 = pts[f'lcv_green_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b3 = pts[f'lcv_red_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b4 = pts[f'lcv_nir_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b5 = pts[f'lcv_swir1_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b6 = pts[f'lcv_swir2_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()
            b7 = pts[f'lcv_thermal_landsat.glad.ard_p{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'].to_numpy()

            pts[f'lcv_ndvi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = ndvi(b3,b4)
            pts[f'lcv_evi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = evi(b4,b3,b1)
            pts[f'lcv_savi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = savi(b4,b3)
            pts[f'lcv_msavi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = msavi(b4,b3)
            pts[f'lcv_ndmi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = ndmi(b4,b5)
            pts[f'lcv_nbr_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = nbr(b4,b5)
            pts[f'lcv_nbr2_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = nbr2(b5,b7)
            pts[f'lcv_rei_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = rei(b1,b5,b4)
            pts[f'lcv_ndwi_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v{ls_version}'] = ndwi(b2,b5)
    
    return(pts)


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('path_points')
    p.add_argument('--dir_output',default=None)
    p.add_argument('--path_output',default=None)
    p.add_argument('--no_filter',action='store_true')
    p.add_argument('--year',default=None)
    p.add_argument('--dir_cop',default='/mnt/diskstation/eu_data/OSM+/Copernicus/')
    p.add_argument('--column_target',default='clc3_orig')
    p.add_argument('--column_date',default='survey_date')
    p.add_argument('--save_unfiltered',action='store_true')
    p.add_argument('--path_points_unfiltered',default=None)
    p.add_argument('--landsat_version',default='1.1')
    p.add_argument('--dtm_version',default='0.2')
    p.add_argument('--hyd_version',default='0.1')
    p.add_argument('--only_filter',action='store_true')
    p.add_argument('--path_tiles',default='/mnt/diskstation/geoharmonizer/LUCAS_CLC/eu_tiling system_30km.gpkg')
    a = p.parse_args()
    
    
    dir_points = os.path.split(a.path_points)[0]
    
    
    
    if a.dir_output is None:
        a.dir_output = dir_points
    print(f"Output will be stored in {a.dir_output}")
    if a.path_points.endswith("unfiltered.joblib") or a.only_filter:
        name_unfiltered = os.path.splitext(os.path.split(a.path_points)[1])[0]
        name_pts = name_unfiltered[:-18]+"filtered.joblib"
        name_output = name_pts
    else:
        name_pts = os.path.splitext(os.path.split(a.path_points)[1])[0]
        name_output = f"{name_pts}_preprocessed_lsv{a.landsat_version}_dtmv{a.dtm_version}_hydv{a.hyd_version}.joblib"
        name_unfiltered = f"{name_pts}_preprocessed_lsv{a.landsat_version}_dtmv{a.dtm_version}_hydv{a.hyd_version}_unfiltered.joblib"
    
    if a.path_output is None:
        a.path_output = os.path.join(a.dir_output,name_output)
    
    ttprint(f"Preprocessed points will be stored as {a.path_output}")
    
    ttprint(f"Loading {a.path_points}")
    pts = load_pts(a.path_points)
    pts['clc3_orig'] = pts['clc3_orig'].astype(int)
    

    if a.year is not None:
        pts[a.column_date] = a.year

    if a.path_points.endswith("unfiltered.joblib"):
        ttprint("Unfiltered point dataset detected; will only run filter")
        
    else:
        ttprint(f"Removing invalid dates")
        
        pts = remove_bad_dates(pts,a.column_date)
        
        
        ttprint(f"Adding tile_id to enable spatial cross_validation")
        pts = add_tile_ids(pts,a.path_tiles)
        
        ttprint("Overlaying points with covariate data")
        first_covariate_index = len(pts.columns)
        pts = overlay(pts,a.column_date,
                      ls_version=a.landsat_version,
                      dtm_version=a.dtm_version, 
                      hyd_version=a.hyd_version,
                      verbose=True)
        
        
        ttprint("Adding geo temp")
        pts = add_geo_temp(pts,dtm_version=a.dtm_version)

        ttprint("Adding spectral indices")
        pts = add_indices(pts,ls_version=a.landsat_version)
        last_covariate_index = len(pts.columns)
        
    if not a.no_filter:
        ttprint("Overlaying points with OSM and Copernicus data for filtering")
        pts = filter_overlay(pts,dir_cop=a.dir_cop)

    ttprint("Removing points with NA values in the target or covariate columns")
    pts = remove_na_points(pts, first_covariate_index, last_covariate_index, a.column_target)

    if not a.no_filter:
        ttprint("Filtering points")
        if a.save_unfiltered:
            if a.path_points_unfiltered is None:
                a.path_points_unfiltered = os.path.join(a.dir_output,name_unfiltered)


            ttprint(f"Unfiltered points will be stored at {a.path_points_unfiltered}")
            joblib.dump(pts,a.path_points_unfiltered,compress='lz4')
        
    
        pts,filter_report = filter_points(pts)
    
    
    
    joblib.dump(pts,a.path_output,compress='lz4')
    ttprint(f'Preprocessed points stored at {a.path_output}')
    
    



