import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(1) 
tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.set_soft_device_placement(True)

import sys

import os
import gdal
import traceback
from pathlib import Path
import numpy as np
import rasterio

import pandas as pd
import geopandas as gpd
import joblib

from sklearn.metrics import ConfusionMatrixDisplay, classification_report, f1_score
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor

from uuid import uuid4
from minio import Minio
from minio.error import S3Error

import numexpr as ne

import math
from datetime import datetime 
from pyproj import Proj, transform

from eumap.misc import ttprint
from eumap.mapper import LandMapper
from eumap.mapper import PredictionStrategyType
from eumap.parallel import TilingProcessing
from eumap.datasets import pilot
import rasterio

from eumap.misc import find_files
from tqdm import tqdm

from joblib import Parallel, delayed
import sys

def parprint(x):
  stream = getattr(sys, "stdout")
  print("{}".format(x), file=stream)
  stream.flush()
  return x

def s3_urls(client, host, bucket_name, prefix, ignore=['_flag']):
  urls = []
  
  for images in client.list_objects(bucket_name, prefix=prefix):
    should_append = True
    
    for i in ignore:
      if i in images.object_name:
        should_append = False
        break
    if should_append:
      urls.append(Path(f'http://{host}/{bucket_name}{images.object_name}'))
  
  return urls

def geotemp_imgs(idx, tile, window, base_raster_fn, elev_fn):
    print("geotemp_imgs")

    def geo_temp(fi, day, a=37.03043, b=-15.43029):
        f =fi
        pi = math.pi 

        #math.cos((day - 18) * math.pi / 182.5 + math.pow(2, (1 - math.copysign(1, fi))) * math.pi) 
        sign = 'where(abs(fi) - fi == 0, 1, -1)'
        costeta = f"cos((day - 18) * pi / 182.5 + 2**(1 - {sign}) * pi)"

        #math.cos(fi * math.pi / 180)
        cosfi = "cos(fi * pi / 180)"
        A = cosfi

        #(1 - costeta) * abs(math.sin(fi * math.pi / 180) )
        B = f"(1 - {costeta}) * abs(sin(fi * pi / 180) )"

        x = f"a * {A} + b * {B}"
        return ne.evaluate(x)

    max_temp_arr = []
    min_temp_arr = []

    with rasterio.open(elev_fn) as ds:

        window.round_offsets()
        pixel_size = ds.transform[0]
        lon = np.arange(window.col_off, window.col_off+window.width)
        lat = np.arange(window.row_off, window.row_off+window.height)

        elev_corr = 0.006 * ds.read(1, window=window)

        inProj = Proj(init='epsg:3035')
        outProj = Proj(init='epsg:4326')

        lon_grid_3035, lat_grid_3035 = ds.transform * np.meshgrid(lon, lat)
        lon_grid_4326, lat_grid_4326 = transform(inProj, outProj, lon_grid_3035, lat_grid_3035)

        for m in range(1,13):
            doy = (datetime.strptime(f'2000-{m}-15', '%Y-%m-%d').timetuple().tm_yday)
            max_temp_arr.append( geo_temp(lat_grid_4326, day=doy, a=37.03043, b=-15.43029) - elev_corr )
            min_temp_arr.append( geo_temp(lat_grid_4326, day=doy, a=24.16453, b=-15.71751) - elev_corr )

    return np.stack(max_temp_arr, axis=2), np.stack(min_temp_arr, axis=2)

def spectral_indices_imgs(layernames, layers_data, indices = ['ndvi','rei','ndwi','nbr','nbr2','ndmi','msavi','savi', 'evi']):

  ## spectral indicies functions:
  def ndvi(b3, b4):
    data = ne.evaluate('(b4/255 - b3/255) / (b3/255 + b4/255)')
    data[~np.isfinite(data)] = 0
    return data

  #source: https://www.usgs.gov/core-science-systems/nli/landsat/landsat-enhanced-vegetation-index?qt-science_support_page_related_con=0#qt-science_support_page_related_con
  def evi(b4, b3, b1):
    data = ne.evaluate('2.5 * ((b4/255 - b3/255) / (b4/255 + 6 * b3/255 - 7.5/255 * b1 + 1))')
    data[~np.isfinite(data)] = 0
    return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-soil-adjusted-vegetation-index
  def savi(b4, b3):
    data = ne.evaluate('((b4/255 - b3/255) / (b4/255 + b3/255 + 0.5)) * (1.5)')
    data[~np.isfinite(data)] = 0
    return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-modified-soil-adjusted-vegetation-index
  def msavi(b4, b3):
    data = ne.evaluate('(2 * b4/255 + 1 - sqrt(((2 * b4/255 + 1)**2) - (8 * (b4/255 - b3/255)))) / 2')
    data[~np.isfinite(data)] = 0
    return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/normalized-difference-moisture-index
  def ndmi(b4, b5):
    data = ne.evaluate('(b4/255 - b5/255) / (b4/255 + b5/255)')
    data[~np.isfinite(data)] = 0
    return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio
  def nbr(b4, b7):
    data = ne.evaluate('(b4/255 - b7/255) / (b4/255 + b7/255)')
    data[~np.isfinite(data)] = 0
    return data

  #https://www.usgs.gov/core-science-systems/nli/landsat/landsat-normalized-burn-ratio-2
  def nbr2(b5, b7):
    data = ne.evaluate('(b5/255 - b7/255) / (b5/255 + b7/255)')
    data[~np.isfinite(data)] = 0
    return data

  def rei(b1, b5, b4):
    data = ne.evaluate('(b4/255 - b1/255) / (b5/255 + (b1/255 * b5/255))')
    data[~np.isfinite(data)] = 0
    return data

  def ndwi(b2, b5):
    data = ne.evaluate('((b2/255 - b5/255) / (b2/255 + b5/255))')
    data[~np.isfinite(data)] = 0
    return data

  def str2idx(name):
    for i in range(0,len(layernames)):
      if (name == layernames[i]):
        return i
    return -1000

  result_names = []
  result_data = []
  
  seasons = ['.03.21...06.24','.06.25...09.12','.09.13...12.01','.12.02...03.20']
  pcts = ['p25','p50','p75']
    
  for perc in pcts:
    for season in seasons:
      b1 = f'lcv_blue_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b2 = f'lcv_green_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b3 = f'lcv_red_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b4 = f'lcv_nir_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b5 = f'lcv_swir1_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b6 = f'lcv_swir2_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'
      b7 = f'lcv_thermal_landsat.glad.ard_{perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1'

      _perc = perc.replace('p','')
    
      if 'ndvi' in indices:
        result_data.append(ndvi(layers_data[:,:,str2idx(b3)], layers_data[:,:,str2idx(b4)]))
        result_names.append(f'lcv_ndvi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'evi' in indices:
        result_data.append(evi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)], layers_data[:,:,str2idx(b1)]))
        result_names.append(f'lcv_evi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'savi' in indices:
        result_data.append(savi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)]))
        result_names.append(f'lcv_savi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'msavi' in indices:
        result_data.append(msavi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b3)]))
        result_names.append(f'lcv_msavi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'ndmi' in indices:
        result_data.append(ndmi(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b5)]))
        result_names.append(f'lcv_ndmi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'nbr' in indices:
        result_data.append(nbr(layers_data[:,:,str2idx(b4)], layers_data[:,:,str2idx(b7)]))
        result_names.append(f'lcv_nbr_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'nbr2' in indices:
        result_data.append(nbr2(layers_data[:,:,str2idx(b5)], layers_data[:,:,str2idx(b7)]))
        result_names.append(f'lcv_nbr2_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'rei' in indices:
        result_data.append(rei(layers_data[:,:,str2idx(b1)], layers_data[:,:,str2idx(b5)], layers_data[:,:,str2idx(b4)]))
        result_names.append(f'lcv_rei_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

      if 'ndwi' in indices:
        result_data.append(ndwi(layers_data[:,:,str2idx(b2)], layers_data[:,:,str2idx(b5)]))
        result_names.append(f'lcv_ndwi_landsat.glad.ard_{_perc}_30m_0..0cm_{season}_eumap_epsg3035_v1.1')

  return result_names, np.stack(result_data, axis=2)

def build_ann_2(input_shape, output_shape, 
                n_layers = 3, n_neurons = 32, activation = 'relu',
                dropout_rate = 0.0, learning_rate = 0.0001,
                output_activation = 'softmax', loss = 'categorical_crossentropy'):
    try:
        from tensorflow.keras.layers import Dense, BatchNormalization, Dropout
        from tensorflow.keras.models import Sequential
        from tensorflow.keras.optimizers import Nadam
    
    except ImportError as e:
        warnings.warn('build_ann requires tensorflow>=2.5.0')

    model = Sequential()
    model.add(Dense(input_shape, activation=activation))

    for i in range(0, n_layers):
        model.add(Dense(n_neurons, activation=activation))
        model.add(Dropout(dropout_rate))
        model.add(BatchNormalization())

    model.add(Dense(output_shape, activation=output_activation))
    model.compile(loss=loss,optimizer=Nadam(learning_rate=learning_rate))
    return model

def process_tile(idx, tile, window, base_raster_fn, landmapper, 
                 s3_host, s3_access_key, s3_access_secret, s3_bucket_name,
                 inmem=['ndvi','rei','ndwi','nbr','nbr2','ndmi','msavi','savi', 'evi', 'max.geom.temp', 'min.geom.temp']):
    
    def inmem_calc(layernames, layers_data, spatial_win):

        layers_data = np.append(layers_data, max_temp_img, axis=2)
        layernames += [f'clm_lst_max.geom.temp_m_30m_s0..0cm_m{m}' for m in range(1,13)]

        layers_data = np.append(layers_data, min_temp, axis=2)
        layernames += [f'clm_lst_min.geom.temp_m_30m_s0..0cm_m{m}' for m in range(1,13)]

        indices_layers, indices_data = spectral_indices_imgs(layernames, layers_data)
        
        layers_data = np.append(layers_data, indices_data, axis=2)
        layernames += indices_layers
        
        return layernames, layers_data

    # Necessary to avoid reading tiles with diferent sizes
    window = window.round_offsets()

    tmp_dir = f'result/{idx}'

    landsat_pref = f'/lcv'
    night_lights_pref = f'/lcv/lcv_night.light_suomi.npp.viirs_avg.rade9h_30m_0..0cm'
    dtm_pref = f'/dtm/dtm'
    hyd_pref = f'/hyd/hyd'
    out_pref = f'tmp/eumap/lc_r4/{idx}'

    Path(tmp_dir).mkdir(parents=True, exist_ok=True)
    client = Minio(s3_host, s3_access_key, s3_access_secret, secure=False)

    urls = s3_urls(client, s3_host, s3_bucket_name, f'/{out_pref}/')
    if False and len(urls) == 20:
        ttprint(f'Ignoring tile {idx}. Already processed.')
        return
    
    years = range(2000,2021)
    
    fn_result_list = []
    
    dict_layers_newnames_list = [{} for i in range(len(years))]
    
    timeless_urls = []
    temporal_urls = [[] for i in range(len(years))]
    
    # This loop must be optimized avoiding check the URLs in S3
    for feature in tqdm(landmapper.feature_cols):
      if any([ im in feature for im in inmem ]):
        pass
      else:
        urls = s3_urls(client, s3_host, s3_bucket_name, f"/{feature.split('_')[0]}/{feature}")
        if len(urls):
          timeless_urls.append(urls[0])
          if feature.startswith('dtm_elev.lowestmode'):
            elev_fn = urls[0]
        else:
          date_col = feature.split("_")[6]
          date_col = date_col[0:len(date_col)//2]
          
          for i in range(len(years)):
            year = years[i]
            if date_col.startswith(".12") or date_col.startswith("12"):
              year = years[i-1]
            
            year_col = f"{year}{date_col}"
            criterion = f"{'_'.join(feature.split('_')[:6])}_{year_col}"
            
            _feat = feature
            if 'clm_lst_mod11a2' in feature: 
                _feat = _feat.replace('_1km_','_30m_')
            
            urls =  s3_urls(client, s3_host, s3_bucket_name, f"/{_feat.split('_')[0]}/{'_'.join(_feat.split('_')[:6])}_{year_col}")
            if len(urls):
              dict_layers_newnames_list[i][feature] = urls[0].stem
              temporal_urls[i].append(urls[0])
    
    
    try:
      ttprint('Generating geometric temperature layers on the fly')
      max_temp_img, min_temp = geotemp_imgs(idx, tile, window, base_raster_fn, elev_fn)
      
      fn_layers_list = []
      for i in range(len(years)):
        fn_layers_list.append(timeless_urls + temporal_urls[i])
        fn_result_list.append(os.path.join(tmp_dir,f'lc_{years[i]}.tif'))

      output_fn_files = landmapper.predict(fn_layers=fn_layers_list[10], fn_output=fn_result_list[10], spatial_win=window, \
                          inmem_calc_func=inmem_calc, dict_layers_newnames=dict_layers_newnames_list[10], \
                          allow_additional_layers=True, hard_class=True, verbose_renaming=False)

      #output_fn_files = landmapper.predict_multi(fn_layers_list=fn_layers_list, fn_output_list=fn_result_list, spatial_win=window, \
      #                                           inmem_calc_func=inmem_calc, dict_layers_newnames_list=dict_layers_newnames_list, \
      #                                           allow_additional_layers=True, hard_class=True, 
      #                                           prediction_strategy_type = PredictionStrategyType.Lazy)

      for output_fn_file in output_fn_files:
        object_name = f'{Path(output_fn_file).name}'
        object_bucket = f'{s3_bucket_name}'
        ttprint(f'Copying {output_fn_file} to http://{s3_host}/{object_bucket}/{out_pref}/{object_name}')
        client.fput_object(object_bucket, f'{out_pref}/{object_name}', output_fn_file)
        os.remove(output_fn_file)

    except:
      tb = traceback.format_exc()
      ttprint(f'ERROR: Tile {idx} failed.')
      ttprint(tb)

model_fn = "landmapper_points_preprocessed_lsv1.1_dtmv0.2_filtered_2021.09.17.joblib"
landmapper = LandMapper.load_instance(model_fn)

# S3/Min.io credentials
s3_host = "192.168.1.57:9000" # Gaia server
s3_access_key = ""
s3_access_secret = ""
s3_bucket_name = 'eumap'

if s3_access_key == "" or s3_access_secret == "":
  print("You need setup the S3 credentials in s3_access_key and s3_access_secret variables")
  exit()

base_raster_fn = f'http://{s3_host}/{s3_bucket_name}/lcv/lcv_red_landsat.glad.ard_p50_30m_0..0cm_19991202..20000320_eumap_epsg3035_v1.0.tif'
processing = TilingProcessing(base_raster_fn=base_raster_fn)
func_args = []

if s3_access_key == "" or s3_access_secret == "":
  print("You need setup the S3 credentials in s3_access_key and s3_access_secret variables")

        
########################################################################
### Test one tile.tiling
########################################################################
dict_test = processing.process_one(625, 
                        process_tile, 
                        base_raster_fn, 
                        landmapper, 
                        s3_host, 
                        s3_access_key, 
                        s3_access_secret, 
                        s3_bucket_name)