import pystac
from eumap.misc import GoogleSheet
from eumap.datasets.eo import STACGenerator

def add_qa_information(gsheet):
	for index, r in gsheet.collections.iterrows():
		qaInfo = gsheet.QA[gsheet.QA['dataset_id'] == r['dataset_id']]
		description = f"Overview:\n{r['description']}\n"
		for column in qaInfo.columns[2:]:
			if len(list(qaInfo[column])) > 0:
				coulmn_val = list(qaInfo[column])[0]
				if coulmn_val != 'n/a':
					description += f"\n{column}:\n{list(qaInfo[column])[0]}\n"
		gsheet.collections.loc[index, 'description'] = description

key_file = '<GDRIVE_KEY>'
url = 'https://docs.google.com/spreadsheets/d/1MG68AY47wFsm1-pf6RGGe-2Cfy1-NiPqXMfi9SF4Bsg'

gsheet = GoogleSheet(key_file, url, verbose=True)
add_qa_information(gsheet)

s3_host = "s3.eu-central-1.wasabisys.com"
s3_access_key = "<s3_access_key>"
s3_access_secret = "<s3_access_secret>"
s3_bucket_name = 'stac'

# Create a new catalog and upload to s3
stac_generator.save_and_publish_all(s3_host, s3_access_key, s3_access_secret, s3_bucket_name, output_dir='stac_odse')

# Uncoment to update specific datasets and / or
# a pre-existing catalog
"""
gsheet.collections = gsheet.collections[gsheet.collections['dataset_id'] == 'lcv_sentinel_eumap']

catalogs = {
	'odse': pystac.Catalog.from_file('./stac_odse/odse/catalog.json')
}

stac_generator = STACGenerator(gsheet, asset_id_fields=[1,2,3,5], catalogs=catalogs, verbose=True)
thumb_base_url=f'https://{s3_host}/{s3_bucket_name}'
stac_generator.save_all(output_dir='stac_odse', thumb_base_url=thumb_base_url)
"""