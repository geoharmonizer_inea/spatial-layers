<ns0:StyledLayerDescriptor xmlns:ns0="http://www.opengis.net/sld" xmlns:ns1="http://www.opengis.net/ogc" version="1.0.0">
  <ns0:UserLayer>
    <ns0:LayerFeatureConstraints>
      <ns0:FeatureTypeConstraint />
    </ns0:LayerFeatureConstraints>
    <ns0:UserStyle>
      <ns0:Name>GRASS color table</ns0:Name>
      <ns0:Title>Celsius x 10</ns0:Title>
      <ns0:FeatureTypeStyle>
        <ns0:Name />
        <ns0:Rule>
          <ns0:RasterSymbolizer>
            <ns0:Geometry>
              <ns1:PropertyName>grid</ns1:PropertyName>
            </ns0:Geometry>
            <ns0:Opacity>1</ns0:Opacity>
            <ns0:ColorMap>  

              <ns0:ColorMapEntry color="#000028" opacity="1" quantity="-800" label="-800"/>
              <ns0:ColorMapEntry color="#5b0aa8" opacity="1" quantity="-400" label="-400"/>
              <ns0:ColorMapEntry color="#dcdcdc" opacity="1" quantity="-300" label="-300"/>
              <ns0:ColorMapEntry color="#5b3280" opacity="1" quantity="-250" label="-250"/>
              <ns0:ColorMapEntry color="#320096" opacity="1" quantity="-200" label="-200"/>
              <ns0:ColorMapEntry color="#041982" opacity="1" quantity="-150" label="-150"/>
              <ns0:ColorMapEntry color="#08366a" opacity="1" quantity="-100" label="-100"/>
              <ns0:ColorMapEntry color="#041496" opacity="1" quantity="-80" label="-80"/>
              <ns0:ColorMapEntry color="#0032ff" opacity="1" quantity="-50" label="-50"/>
              <ns0:ColorMapEntry color="#080a76" opacity="1" quantity="-20" label="-20"/>
              <ns0:ColorMapEntry color="#08719b" opacity="1" quantity="-0.61449" label="-0.61"/>
              <ns0:ColorMapEntry color="#08769d" opacity="1" quantity="0.13855" label="0.14"/>
              <ns0:ColorMapEntry color="#087b9e" opacity="1" quantity="0.90594" label="0.91"/>
              <ns0:ColorMapEntry color="#08809f" opacity="1" quantity="1.68767" label="1.69"/>
              <ns0:ColorMapEntry color="#08859f" opacity="1" quantity="2.48375" label="2.48"/>
              <ns0:ColorMapEntry color="#0888a0" opacity="1" quantity="3.29417" label="3.29"/>
              <ns0:ColorMapEntry color="#088ca2" opacity="1" quantity="4.11893" label="4.12"/>
              <ns0:ColorMapEntry color="#0891a3" opacity="1" quantity="4.95804" label="4.96"/>
              <ns0:ColorMapEntry color="#0896a5" opacity="1" quantity="5.81149" label="5.81"/>
              <ns0:ColorMapEntry color="#089ca6" opacity="1" quantity="6.67928" label="6.68"/>
              <ns0:ColorMapEntry color="#08a1a7" opacity="1" quantity="7.56142" label="7.56"/>
              <ns0:ColorMapEntry color="#08a6a9" opacity="1" quantity="8.4579" label="8.46"/>
              <ns0:ColorMapEntry color="#08acac" opacity="1" quantity="9.36873" label="9.37"/>
              <ns0:ColorMapEntry color="#08b1b1" opacity="1" quantity="10.2939" label="10.29"/>
              <ns0:ColorMapEntry color="#08b6b6" opacity="1" quantity="11.2334" label="11.23"/>
              <ns0:ColorMapEntry color="#08bbbb" opacity="1" quantity="12.1873" label="12.19"/>
              <ns0:ColorMapEntry color="#08c0c0" opacity="1" quantity="13.1555" label="13.16"/>
              <ns0:ColorMapEntry color="#08c5c5" opacity="1" quantity="14.138" label="14.14"/>
              <ns0:ColorMapEntry color="#08c8c8" opacity="1" quantity="15.1349" label="15.13"/>
              <ns0:ColorMapEntry color="#08cccc" opacity="1" quantity="16.1461" label="16.15"/>
              <ns0:ColorMapEntry color="#08d1d1" opacity="1" quantity="17.1717" label="17.17"/>
              <ns0:ColorMapEntry color="#08d5d5" opacity="1" quantity="18.2116" label="18.21"/>
              <ns0:ColorMapEntry color="#08d8d8" opacity="1" quantity="19.2659" label="19.27"/>
              <ns0:ColorMapEntry color="#08dbdb" opacity="1" quantity="20.3345" label="20.33"/>
              <ns0:ColorMapEntry color="#08dede" opacity="1" quantity="21.4174" label="21.42"/>
              <ns0:ColorMapEntry color="#08e1e1" opacity="1" quantity="22.5147" label="22.51"/>
              <ns0:ColorMapEntry color="#08e3e3" opacity="1" quantity="23.6264" label="23.63"/>
              <ns0:ColorMapEntry color="#08e6e6" opacity="1" quantity="24.7523" label="24.75"/>
              <ns0:ColorMapEntry color="#08e9e9" opacity="1" quantity="25.8927" label="25.89"/>
              <ns0:ColorMapEntry color="#08ebeb" opacity="1" quantity="27.0473" label="27.05"/>
              <ns0:ColorMapEntry color="#08edec" opacity="1" quantity="28.2164" label="28.22"/>
              <ns0:ColorMapEntry color="#08eeea" opacity="1" quantity="29.3997" label="29.4"/>
              <ns0:ColorMapEntry color="#08eee5" opacity="1" quantity="30.5974" label="30.6"/>
              <ns0:ColorMapEntry color="#08eedf" opacity="1" quantity="31.8094" label="31.81"/>
              <ns0:ColorMapEntry color="#08edd8" opacity="1" quantity="33.0358" label="33.04"/>
              <ns0:ColorMapEntry color="#08ebd1" opacity="1" quantity="34.2766" label="34.28"/>
              <ns0:ColorMapEntry color="#08eaca" opacity="1" quantity="35.5316" label="35.53"/>
              <ns0:ColorMapEntry color="#08e8c0" opacity="1" quantity="36.8011" label="36.8"/>
              <ns0:ColorMapEntry color="#08e7b2" opacity="1" quantity="38.0848" label="38.08"/>
              <ns0:ColorMapEntry color="#08e6a2" opacity="1" quantity="39.3829" label="39.38"/>
              <ns0:ColorMapEntry color="#08e593" opacity="1" quantity="40.6954" label="40.7"/>
              <ns0:ColorMapEntry color="#08e385" opacity="1" quantity="42.0222" label="42.02"/>
              <ns0:ColorMapEntry color="#08e27b" opacity="1" quantity="43.3633" label="43.36"/>
              <ns0:ColorMapEntry color="#08e074" opacity="1" quantity="44.7188" label="44.72"/>
              <ns0:ColorMapEntry color="#08dd72" opacity="1" quantity="46.0886" label="46.09"/>
              <ns0:ColorMapEntry color="#08db72" opacity="1" quantity="47.4728" label="47.47"/>
              <ns0:ColorMapEntry color="#08d873" opacity="1" quantity="48.8713" label="48.87"/>
              <ns0:ColorMapEntry color="#08d575" opacity="1" quantity="50.2841" label="50.28"/>
              <ns0:ColorMapEntry color="#08d176" opacity="1" quantity="51.7113" label="51.71"/>
              <ns0:ColorMapEntry color="#08cc76" opacity="1" quantity="53.1529" label="53.15"/>
              <ns0:ColorMapEntry color="#08c775" opacity="1" quantity="54.6088" label="54.61"/>
              <ns0:ColorMapEntry color="#08c173" opacity="1" quantity="56.079" label="56.08"/>
              <ns0:ColorMapEntry color="#08bc70" opacity="1" quantity="57.5636" label="57.56"/>
              <ns0:ColorMapEntry color="#08b86b" opacity="1" quantity="59.0625" label="59.06"/>
              <ns0:ColorMapEntry color="#08b563" opacity="1" quantity="60.5757" label="60.58"/>
              <ns0:ColorMapEntry color="#08b25b" opacity="1" quantity="62.1033" label="62.1"/>
              <ns0:ColorMapEntry color="#08af54" opacity="1" quantity="63.6453" label="63.65"/>
              <ns0:ColorMapEntry color="#08ac51" opacity="1" quantity="65.2016" label="65.2"/>
              <ns0:ColorMapEntry color="#08a950" opacity="1" quantity="66.7722" label="66.77"/>
              <ns0:ColorMapEntry color="#08a450" opacity="1" quantity="68.3572" label="68.36"/>
              <ns0:ColorMapEntry color="#089f50" opacity="1" quantity="69.9565" label="69.96"/>
              <ns0:ColorMapEntry color="#089950" opacity="1" quantity="71.5702" label="71.57"/>
              <ns0:ColorMapEntry color="#08944f" opacity="1" quantity="73.1982" label="73.2"/>
              <ns0:ColorMapEntry color="#088f4f" opacity="1" quantity="74.8405" label="74.84"/>
              <ns0:ColorMapEntry color="#088a4f" opacity="1" quantity="76.4972" label="76.5"/>
              <ns0:ColorMapEntry color="#08864e" opacity="1" quantity="78.1683" label="78.17"/>
              <ns0:ColorMapEntry color="#08844b" opacity="1" quantity="79.8537" label="79.85"/>
              <ns0:ColorMapEntry color="#088646" opacity="1" quantity="81.5534" label="81.55"/>
              <ns0:ColorMapEntry color="#08893f" opacity="1" quantity="83.2675" label="83.27"/>
              <ns0:ColorMapEntry color="#088c38" opacity="1" quantity="84.9959" label="85"/>
              <ns0:ColorMapEntry color="#088c30" opacity="1" quantity="86.7386" label="86.74"/>
              <ns0:ColorMapEntry color="#088b25" opacity="1" quantity="88.4958" label="88.5"/>
              <ns0:ColorMapEntry color="#098a18" opacity="1" quantity="90.2672" label="90.27"/>
              <ns0:ColorMapEntry color="#0e8b0e" opacity="1" quantity="92.053" label="92.05"/>
              <ns0:ColorMapEntry color="#188d09" opacity="1" quantity="93.8531" label="93.85"/>
              <ns0:ColorMapEntry color="#259208" opacity="1" quantity="95.6676" label="95.67"/>
              <ns0:ColorMapEntry color="#2f9708" opacity="1" quantity="97.4964" label="97.5"/>
              <ns0:ColorMapEntry color="#349c08" opacity="1" quantity="99.3396" label="99.34"/>
              <ns0:ColorMapEntry color="#37a108" opacity="1" quantity="101.197" label="101.2"/>
              <ns0:ColorMapEntry color="#39a608" opacity="1" quantity="103.069" label="103.07"/>
              <ns0:ColorMapEntry color="#3cac08" opacity="1" quantity="104.955" label="104.96"/>
              <ns0:ColorMapEntry color="#41b108" opacity="1" quantity="106.856" label="106.86"/>
              <ns0:ColorMapEntry color="#46b608" opacity="1" quantity="108.771" label="108.77"/>
              <ns0:ColorMapEntry color="#4bbb08" opacity="1" quantity="110.7" label="110.7"/>
              <ns0:ColorMapEntry color="#50c008" opacity="1" quantity="112.643" label="112.64"/>
              <ns0:ColorMapEntry color="#55c408" opacity="1" quantity="114.601" label="114.6"/>
              <ns0:ColorMapEntry color="#59c708" opacity="1" quantity="116.574" label="116.57"/>
              <ns0:ColorMapEntry color="#5fc908" opacity="1" quantity="118.56" label="118.56"/>
              <ns0:ColorMapEntry color="#67cc08" opacity="1" quantity="120.561" label="120.56"/>
              <ns0:ColorMapEntry color="#6ece08" opacity="1" quantity="122.576" label="122.58"/>
              <ns0:ColorMapEntry color="#76d108" opacity="1" quantity="124.606" label="124.61"/>
              <ns0:ColorMapEntry color="#7ed308" opacity="1" quantity="126.65" label="126.65"/>
              <ns0:ColorMapEntry color="#86d608" opacity="1" quantity="128.708" label="128.71"/>
              <ns0:ColorMapEntry color="#8ed908" opacity="1" quantity="130.781" label="130.78"/>
              <ns0:ColorMapEntry color="#97db08" opacity="1" quantity="132.868" label="132.87"/>
              <ns0:ColorMapEntry color="#a0de08" opacity="1" quantity="134.969" label="134.97"/>
              <ns0:ColorMapEntry color="#a9e108" opacity="1" quantity="137.085" label="137.09"/>
              <ns0:ColorMapEntry color="#b3e308" opacity="1" quantity="139.215" label="139.22"/>
              <ns0:ColorMapEntry color="#bbe508" opacity="1" quantity="141.359" label="141.36"/>
              <ns0:ColorMapEntry color="#c3e608" opacity="1" quantity="143.518" label="143.52"/>
              <ns0:ColorMapEntry color="#cde608" opacity="1" quantity="145.691" label="145.69"/>
              <ns0:ColorMapEntry color="#d7e608" opacity="1" quantity="147.879" label="147.88"/>
              <ns0:ColorMapEntry color="#dfe508" opacity="1" quantity="150.08" label="150.08"/>
              <ns0:ColorMapEntry color="#e2e308" opacity="1" quantity="152.297" label="152.3"/>
              <ns0:ColorMapEntry color="#e1e008" opacity="1" quantity="154.527" label="154.53"/>
              <ns0:ColorMapEntry color="#e0dd08" opacity="1" quantity="156.772" label="156.77"/>
              <ns0:ColorMapEntry color="#e0d908" opacity="1" quantity="159.031" label="159.03"/>
              <ns0:ColorMapEntry color="#e0d308" opacity="1" quantity="161.304" label="161.3"/>
              <ns0:ColorMapEntry color="#e0cd08" opacity="1" quantity="163.592" label="163.59"/>
              <ns0:ColorMapEntry color="#e0c608" opacity="1" quantity="165.894" label="165.89"/>
              <ns0:ColorMapEntry color="#e0bf08" opacity="1" quantity="168.211" label="168.21"/>
              <ns0:ColorMapEntry color="#e0b908" opacity="1" quantity="170.542" label="170.54"/>
              <ns0:ColorMapEntry color="#e0b408" opacity="1" quantity="172.887" label="172.89"/>
              <ns0:ColorMapEntry color="#e0ae08" opacity="1" quantity="175.246" label="175.25"/>
              <ns0:ColorMapEntry color="#e0a608" opacity="1" quantity="177.62" label="177.62"/>
              <ns0:ColorMapEntry color="#e09f08" opacity="1" quantity="180.009" label="180.01"/>
              <ns0:ColorMapEntry color="#e09708" opacity="1" quantity="182.411" label="182.41"/>
              <ns0:ColorMapEntry color="#e08f08" opacity="1" quantity="184.828" label="184.83"/>
              <ns0:ColorMapEntry color="#e08708" opacity="1" quantity="187.259" label="187.26"/>
              <ns0:ColorMapEntry color="#e07f08" opacity="1" quantity="189.705" label="189.71"/>
              <ns0:ColorMapEntry color="#e07708" opacity="1" quantity="192.165" label="192.17"/>
              <ns0:ColorMapEntry color="#e06f08" opacity="1" quantity="194.639" label="194.64"/>
              <ns0:ColorMapEntry color="#e06708" opacity="1" quantity="197.128" label="197.13"/>
              <ns0:ColorMapEntry color="#e05f08" opacity="1" quantity="199.631" label="199.63"/>
              <ns0:ColorMapEntry color="#e05908" opacity="1" quantity="202.148" label="202.15"/>
              <ns0:ColorMapEntry color="#e05408" opacity="1" quantity="204.68" label="204.68"/>
              <ns0:ColorMapEntry color="#e04e08" opacity="1" quantity="207.226" label="207.23"/>
              <ns0:ColorMapEntry color="#df4608" opacity="1" quantity="209.786" label="209.79"/>
              <ns0:ColorMapEntry color="#df3f08" opacity="1" quantity="212.361" label="212.36"/>
              <ns0:ColorMapEntry color="#df3708" opacity="1" quantity="214.95" label="214.95"/>
              <ns0:ColorMapEntry color="#de2e08" opacity="1" quantity="217.553" label="217.55"/>
              <ns0:ColorMapEntry color="#dd2508" opacity="1" quantity="220.171" label="220.17"/>
              <ns0:ColorMapEntry color="#db1c08" opacity="1" quantity="222.803" label="222.8"/>
              <ns0:ColorMapEntry color="#d81308" opacity="1" quantity="225.449" label="225.45"/>
              <ns0:ColorMapEntry color="#d50c08" opacity="1" quantity="228.11" label="228.11"/>
              <ns0:ColorMapEntry color="#d10908" opacity="1" quantity="230.785" label="230.79"/>
              <ns0:ColorMapEntry color="#cc0808" opacity="1" quantity="233.475" label="233.48"/>
              <ns0:ColorMapEntry color="#c80808" opacity="1" quantity="236.178" label="236.18"/>
              <ns0:ColorMapEntry color="#c40808" opacity="1" quantity="238.897" label="238.9"/>
              <ns0:ColorMapEntry color="#be0808" opacity="1" quantity="241.629" label="241.63"/>
              <ns0:ColorMapEntry color="#b60808" opacity="1" quantity="244.376" label="244.38"/>
              <ns0:ColorMapEntry color="#af0808" opacity="1" quantity="247.137" label="247.14"/>
              <ns0:ColorMapEntry color="#a70808" opacity="1" quantity="249.913" label="249.91"/>
              <ns0:ColorMapEntry color="#9f0808" opacity="1" quantity="252.702" label="252.7"/>
              <ns0:ColorMapEntry color="#970808" opacity="1" quantity="255.507" label="255.51"/>
              <ns0:ColorMapEntry color="#8f0808" opacity="1" quantity="258.325" label="258.33"/>
              <ns0:ColorMapEntry color="#870808" opacity="1" quantity="261.158" label="261.16"/>
              <ns0:ColorMapEntry color="#7f0808" opacity="1" quantity="264.005" label="264.01"/>
              <ns0:ColorMapEntry color="#780808" opacity="1" quantity="266.867" label="266.87"/>
              <ns0:ColorMapEntry color="#730808" opacity="1" quantity="269.743" label="269.74"/>
              <ns0:ColorMapEntry color="#6e0808" opacity="1" quantity="272.633" label="272.63"/>
              <ns0:ColorMapEntry color="#680808" opacity="1" quantity="275.538" label="275.54"/>
              <ns0:ColorMapEntry color="#6c0d0d" opacity="1" quantity="278.457" label="278.46"/>
              <ns0:ColorMapEntry color="#701313" opacity="1" quantity="281.39" label="281.39"/>
              <ns0:ColorMapEntry color="#721616" opacity="1" quantity="284.337" label="284.34"/>
              <ns0:ColorMapEntry color="#731a1a" opacity="1" quantity="287.299" label="287.3"/>
              <ns0:ColorMapEntry color="#761e1e" opacity="1" quantity="290.276" label="290.28"/>
              <ns0:ColorMapEntry color="#772221" opacity="1" quantity="293.266" label="293.27"/>
              <ns0:ColorMapEntry color="#792626" opacity="1" quantity="296.271" label="296.27"/>
              <ns0:ColorMapEntry color="#7b2929" opacity="1" quantity="299.291" label="299.29"/>
              <ns0:ColorMapEntry color="#7d2d2d" opacity="1" quantity="302.324" label="302.32"/>
              <ns0:ColorMapEntry color="#7f3131" opacity="1" quantity="305.372" label="305.37"/>
              <ns0:ColorMapEntry color="#803435" opacity="1" quantity="308.435" label="308.44"/>
              <ns0:ColorMapEntry color="#823838" opacity="1" quantity="311.512" label="311.51"/>
              <ns0:ColorMapEntry color="#843c3c" opacity="1" quantity="314.603" label="314.6"/>
              <ns0:ColorMapEntry color="#86403f" opacity="1" quantity="317.708" label="317.71"/>
              <ns0:ColorMapEntry color="#884343" opacity="1" quantity="320.828" label="320.83"/>
              <ns0:ColorMapEntry color="#8a4746" opacity="1" quantity="323.962" label="323.96"/>
              <ns0:ColorMapEntry color="#8c4b4a" opacity="1" quantity="327.11" label="327.11"/>
              <ns0:ColorMapEntry color="#8e4f4e" opacity="1" quantity="330.273" label="330.27"/>
              <ns0:ColorMapEntry color="#8f5252" opacity="1" quantity="333.45" label="333.45"/>
              <ns0:ColorMapEntry color="#915555" opacity="1" quantity="336.642" label="336.64"/>
              <ns0:ColorMapEntry color="#93595a" opacity="1" quantity="339.848" label="339.85"/>
              <ns0:ColorMapEntry color="#955d5d" opacity="1" quantity="343.068" label="343.07"/>
              <ns0:ColorMapEntry color="#966161" opacity="1" quantity="346.302" label="346.3"/>
              <ns0:ColorMapEntry color="#986464" opacity="1" quantity="349.551" label="349.55"/>
              <ns0:ColorMapEntry color="#9a6868" opacity="1" quantity="352.814" label="352.81"/>
              <ns0:ColorMapEntry color="#9c6b6c" opacity="1" quantity="356.092" label="356.09"/>
              <ns0:ColorMapEntry color="#9e706f" opacity="1" quantity="359.384" label="359.38"/>
              <ns0:ColorMapEntry color="#a07373" opacity="1" quantity="362.69" label="362.69"/>
              <ns0:ColorMapEntry color="#a27777" opacity="1" quantity="366.01" label="366.01"/>
              <ns0:ColorMapEntry color="#a47b7b" opacity="1" quantity="369.345" label="369.35"/>
              <ns0:ColorMapEntry color="#a57e7e" opacity="1" quantity="372.695" label="372.7"/>
              <ns0:ColorMapEntry color="#a88282" opacity="1" quantity="376.058" label="376.06"/>
              <ns0:ColorMapEntry color="#a98686" opacity="1" quantity="379.436" label="379.44"/>
              <ns0:ColorMapEntry color="#aa8a89" opacity="1" quantity="382.828" label="382.83"/>
              <ns0:ColorMapEntry color="#ad8d8d" opacity="1" quantity="386.235" label="386.24"/>
              <ns0:ColorMapEntry color="#af9191" opacity="1" quantity="389.656" label="389.66"/>
              <ns0:ColorMapEntry color="#b09595" opacity="1" quantity="393.091" label="393.09"/>
              <ns0:ColorMapEntry color="#b29999" opacity="1" quantity="396.541" label="396.54"/>
              <ns0:ColorMapEntry color="#b59c9c" opacity="1" quantity="400.005" label="400.01"/>
              <ns0:ColorMapEntry color="#b69f9f" opacity="1" quantity="403.483" label="403.48"/>
              <ns0:ColorMapEntry color="#b7a3a3" opacity="1" quantity="406.976" label="406.98"/>
              <ns0:ColorMapEntry color="#b9a7a7" opacity="1" quantity="410.483" label="410.48"/>
              <ns0:ColorMapEntry color="#bcaaab" opacity="1" quantity="414.005" label="414.01"/>
              <ns0:ColorMapEntry color="#beaeae" opacity="1" quantity="417.54" label="417.54"/>
              <ns0:ColorMapEntry color="#bfb2b2" opacity="1" quantity="421.09" label="421.09"/>
              <ns0:ColorMapEntry color="#c1b6b6" opacity="1" quantity="424.655" label="424.66"/>
              <ns0:ColorMapEntry color="#c3b9b9" opacity="1" quantity="428.233" label="428.23"/>
              <ns0:ColorMapEntry color="#c5bdbd" opacity="1" quantity="431.827" label="431.83"/>
              <ns0:ColorMapEntry color="#c7c0c1" opacity="1" quantity="435.434" label="435.43"/>
              <ns0:ColorMapEntry color="#c8c5c5" opacity="1" quantity="439.056" label="439.06"/>
              <ns0:ColorMapEntry color="#cac8c9" opacity="1" quantity="442.692" label="442.69"/>
              <ns0:ColorMapEntry color="#cccccc" opacity="1" quantity="446.342" label="446.34"/>
              <ns0:ColorMapEntry color="#cecece" opacity="1" quantity="450.007" label="450.01"/>
              <ns0:ColorMapEntry color="#9b0a9b" opacity="1" quantity="800" label="800"/>


            </ns0:ColorMap>
          </ns0:RasterSymbolizer>
        </ns0:Rule>
      </ns0:FeatureTypeStyle>
    </ns0:UserStyle>
  </ns0:UserLayer>
</ns0:StyledLayerDescriptor>
