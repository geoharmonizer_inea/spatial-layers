<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>enq_pm25_et.eml_m_1km_na_2018.01_eumap_3035_v1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry label="0" quantity="0" color="#2b83ba"/>
              <sld:ColorMapEntry label="5" quantity="5" color="#91cba9"/>
              <sld:ColorMapEntry label="10" quantity="10" color="#fdfd6d"/>
              <sld:ColorMapEntry label="15" quantity="15" color="#ffb264"/>
              <sld:ColorMapEntry label="20" quantity="20" color="#f67535"/>
              <sld:ColorMapEntry label="25" quantity="25" color="#ff4e08"/>
              <sld:ColorMapEntry label="30" quantity="30" color="#da2b04"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
