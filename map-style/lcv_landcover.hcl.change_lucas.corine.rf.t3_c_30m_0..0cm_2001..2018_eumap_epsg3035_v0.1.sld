<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.hcl.change_lucas.corine.rf.t3_c_30m_0..0cm_2001..2018_eumap_epsg3035_v0.1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#f294fb" quantity="1" label="Crop expansion"/>
              <sld:ColorMapEntry color="#a7562c" quantity="2" label="Deforestation"/>
              <sld:ColorMapEntry color="#965591" quantity="3" label="Deforestation &amp; crop expansion"/>
              <sld:ColorMapEntry color="#cb0809" quantity="4" label="Deforestation &amp; urbanization"/>
              <sld:ColorMapEntry color="#c7c7c7" quantity="5" label="Desertification"/>
              <sld:ColorMapEntry color="#ffba3b" quantity="6" label="Land abandonment"/>
              <sld:ColorMapEntry color="#d1ad67" quantity="7" label="Land abandonment &amp; desertification"/>
              <sld:ColorMapEntry color="#6e6e6e" quantity="8" label="Other"/>
              <sld:ColorMapEntry color="#008626" quantity="9" label="Reforestation"/>
              <sld:ColorMapEntry color="#ff0d0e" quantity="10" label="Urbanization"/>
              <sld:ColorMapEntry color="#0033c3" quantity="11" label="Water expansion"/>
              <sld:ColorMapEntry color="#ff6565" quantity="12" label="Water reduction"/>
              <sld:ColorMapEntry color="#c98b82" quantity="13" label="Wetland degr. &amp; Desertification"/>
              <sld:ColorMapEntry color="#a385be" quantity="14" label="Wetland degr. &amp; crop expansion"/>
              <sld:ColorMapEntry color="#ff6565" quantity="15" label="Wetland degr. &amp; urbanization"/>
              <sld:ColorMapEntry color="#009082" quantity="16" label="Wetland degradation"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
