This is a very limited raster style converter, able to generate QML from SLD files. It supports color ramps and fixed values as input.

# Usage

It only converts SLD files without an associated QML file with the same name.

```bash
./convert_all.sh
```