<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
<sld:NamedLayer>
    <sld:Name>Example</sld:Name>
    <sld:UserStyle>
        <sld:Name>Example</sld:Name>
        <sld:Title>A Title</sld:Title>
        <sld:FeatureTypeStyle>
            <sld:Name>name</sld:Name>
            <sld:Rule>
                <sld:RasterSymbolizer>
                    <sld:ColorMap type="intervals">
                        <sld:ColorMapEntry color="#ffffff" opacity="1.0" quantity="0.0" label="-"/>
                        <sld:ColorMapEntry color="#ffaa7f" opacity="1.0" quantity="1.0" label="Class 1"/>
                        <sld:ColorMapEntry color="#ff5400" opacity="1.0" quantity="3.0" label="Class 2"/>
                        <sld:ColorMapEntry color="#ff0000" opacity="1.0" quantity="5.0" label="Class 3"/>
                        <sld:ColorMapEntry color="#aa0000" opacity="1.0" quantity="8.0" label="Class 4"/>
                    </sld:ColorMap>
                    <sld:ContrastEnhancement/>
                </sld:RasterSymbolizer>
            </sld:Rule>
        </sld:FeatureTypeStyle>
    </sld:UserStyle>
</sld:NamedLayer>
</sld:StyledLayerDescriptor>
