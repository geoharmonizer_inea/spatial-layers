<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0" version="3.20.2-Odense" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal fetchMode="0" mode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option name="WMSBackgroundLayer" type="QString" value="false"/>
      <Option name="WMSPublishDataSourceUrl" type="QString" value="false"/>
      <Option name="embeddedWidgets/count" type="QString" value="0"/>
      <Option name="identify/format" type="QString" value="Value"/>
    </Option>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" enabled="false" maxOversampling="2" zoomedOutResamplingMethod="nearestNeighbour"/>
    </provider>
    <rasterrenderer nodataColor="" classificationMax="48" type="singlebandpseudocolor" classificationMin="16" alphaBand="-1" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader labelPrecision="0" maximumValue="48" clip="0" colorRampType="INTERPOLATED" classificationMode="1" minimumValue="16">
          <colorramp name="[source]" type="cpt-city">
            <Option type="Map">
              <Option name="inverted" type="QString" value="1"/>
              <Option name="rampType" type="QString" value="cpt-city"/>
              <Option name="schemeName" type="QString" value="wkp/tubs/nrwc"/>
              <Option name="variantName" type="QString" value=""/>
            </Option>
            <prop v="1" k="inverted"/>
            <prop v="cpt-city" k="rampType"/>
            <prop v="wkp/tubs/nrwc" k="schemeName"/>
            <prop v="" k="variantName"/>
          </colorramp>
          <item color="#d77f3f" label="4" alpha="255" value="16"/>
          <item color="#b0783a" label="6" alpha="255" value="19.2"/>
          <item color="#c7a75c" label="8" alpha="255" value="22.4"/>
          <item color="#e7d57a" label="12" alpha="255" value="25.6"/>
          <item color="#a5ba6f" label="17" alpha="255" value="28.8"/>
          <item color="#6ca363" label="24" alpha="255" value="32"/>
          <item color="#3e8a59" label="32" alpha="255" value="35.2"/>
          <item color="#346945" label="44" alpha="255" value="38.4"/>
          <item color="#183e29" label="66" alpha="255" value="41.6"/>
          <item color="#373724" label="89" alpha="255" value="44.8"/>
          <item color="#050603" label="121" alpha="255" value="48"/>
          <rampLegendSettings direction="0" minimumLabel="" maximumLabel="" orientation="2" prefix="" useContinuousLegend="1" suffix="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option name="decimal_separator" type="QChar" value=""/>
                <Option name="decimals" type="int" value="6"/>
                <Option name="rounding_type" type="int" value="0"/>
                <Option name="show_plus" type="bool" value="false"/>
                <Option name="show_thousand_separator" type="bool" value="true"/>
                <Option name="show_trailing_zeros" type="bool" value="false"/>
                <Option name="thousand_separator" type="QChar" value=""/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast gamma="1" brightness="0" contrast="0"/>
    <huesaturation colorizeOn="0" colorizeRed="255" grayscaleMode="0" colorizeStrength="100" saturation="0" colorizeBlue="128" colorizeGreen="128"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
