<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+8" version="3.16.3-Hannover" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" fetchMode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling maxOversampling="2" zoomedOutResamplingMethod="nearestNeighbour" zoomedInResamplingMethod="nearestNeighbour" enabled="false"/>
    </provider>
    <rasterrenderer alphaBand="-1" classificationMax="90" type="singlebandpseudocolor" band="1" opacity="1" classificationMin="10" nodataColor="">
      <rasterTransparency>
        <singleValuePixelList>
          <pixelListEntry min="0" max="5" percentTransparent="100"/>
        </singleValuePixelList>
      </rasterTransparency>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0" labelPrecision="0" maximumValue="90" classificationMode="2" minimumValue="10">
          <colorramp name="[source]" type="preset">
            <prop v="255,204,177,255" k="preset_color_0"/>
            <prop v="153,207,254,255" k="preset_color_1"/>
            <prop v="135,147,99,255" k="preset_color_2"/>
            <prop v="58,82,56,255" k="preset_color_3"/>
            <prop v="&lt; 10%" k="preset_color_name_0"/>
            <prop v="20%" k="preset_color_name_1"/>
            <prop v="40%" k="preset_color_name_2"/>
            <prop v="> 60%" k="preset_color_name_3"/>
            <prop v="preset" k="rampType"/>
          </colorramp>
          <item color="#ffccb1" value="10" label="&lt; 10%" alpha="255"/>
          <item color="#99cffe" value="30" label="30%" alpha="255"/>
          <item color="#879363" value="60" label="60%" alpha="255"/>
          <item color="#3a5238" value="90" label="> 90%" alpha="255"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0" gamma="1"/>
    <huesaturation colorizeStrength="100" colorizeOn="0" colorizeBlue="128" colorizeGreen="128" saturation="0" grayscaleMode="0" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
