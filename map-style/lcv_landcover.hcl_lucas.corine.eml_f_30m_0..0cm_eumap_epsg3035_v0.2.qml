<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" version="3.20.1-Odense" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal enabled="0" mode="0" fetchMode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="bool" value="false" name="WMSBackgroundLayer"/>
      <Option type="bool" value="false" name="WMSPublishDataSourceUrl"/>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option type="QString" value="Value" name="identify/format"/>
    </Option>
  </customproperties>
  <pipe>
    <provider>
      <resampling enabled="false" maxOversampling="2" zoomedInResamplingMethod="nearestNeighbour" zoomedOutResamplingMethod="nearestNeighbour"/>
    </provider>
    <rasterrenderer alphaBand="-1" type="paletted" nodataColor="" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry color='#E6004D' alpha='255' value="1"      label="1 - 111 - Continuous urban fabric"/>
        <paletteEntry color='#FF0000' alpha='255' value="2"       label="2 - 112 - Discontinuous urban fabric"/>
        <paletteEntry color='#CC4DF2' alpha='255' value="3"    label="3 - 121 - Industrial or commercial units"/>
        <paletteEntry color='#CC0000' alpha='255' value="4"       label="4 - 122 - Road and rail networks and associated land"/>
        <paletteEntry color='#E6CCCC' alpha='255' value="5"   label="5 - 123 - Port areas"/>
        <paletteEntry color='#E6CCE6' alpha='255' value="6"   label="6 - 124 - Airports"/>
        <paletteEntry color='#A600CC' alpha='255' value="7"     label="7 - 131 - Mineral extraction sites"/>
        <paletteEntry color='#A64D00' alpha='255' value="8"      label="8 - 132 - Dump sites"/>
        <paletteEntry color='#FF4DFF' alpha='255' value="9"    label="9 - 133 - Construction sites"/>
        <paletteEntry color='#FFA6FF' alpha='255' value="10"  label="10 - 141 - Green urban areas"/>
        <paletteEntry color='#FFE6FF' alpha='255' value="11"  label="11 - 142 - Sport and leisure facilities"/>
        <paletteEntry color='#FFFFA8' alpha='255' value="12"  label="12 - 211 - Non-irrigated arable land"/>
        <paletteEntry color='#FFFF00' alpha='255' value="13"    label="13 - 212 - Permanently irrigated land"/>
        <paletteEntry color='#E6E600' alpha='255' value="14"    label="14 - 213 - Rice fields"/>
        <paletteEntry color='#E68000' alpha='255' value="15"    label="15 - 221 - Vineyards"/>
        <paletteEntry color='#F2A64D' alpha='255' value="16"   label="16 - 222 - Fruit trees and berry plantations"/>
        <paletteEntry color='#E6A600' alpha='255' value="17"    label="17 - 223 - Olive groves"/>
        <paletteEntry color='#E6E64D' alpha='255' value="18"   label="18 - 231 - Pastures"/>
        <paletteEntry color='#FFE6A6' alpha='255' value="19"  label="19 - 241 - Annual crops associated with permanent crops"/>
        <paletteEntry color='#FFE64D' alpha='255' value="20"   label="20 - 242 - Complex cultivation patterns"/>
        <paletteEntry color='#E6CC4D' alpha='255' value="21"   label="21 - 243 - Land principally occupied by agriculture, with significant areas of natural vegetation"/>
        <paletteEntry color='#F2CCA6' alpha='255' value="22"  label="22 - 244 - Agro-forestry areas"/>
        <paletteEntry color='#80FF00' alpha='255' value="23"    label="23 - 311 - Broad-leaved forest"/>
        <paletteEntry color='#00A600' alpha='255' value="24"      label="24 - 312 - Coniferous forest"/>
        <paletteEntry color='#4DFF00' alpha='255' value="25"     label="25 - 313 - Mixed forest"/>
        <paletteEntry color='#CCF24D' alpha='255' value="26"   label="26 - 321 - Natural grasslands"/>
        <paletteEntry color='#A6FF80' alpha='255' value="27"  label="27 - 322 - Moors and heathland"/>
        <paletteEntry color='#A6E64D' alpha='255' value="28"   label="28 - 323 - Sclerophyllous vegetation"/>
        <paletteEntry color='#A6F200' alpha='255' value="29"    label="29 - 324 - Transitional woodland-shrub"/>
        <paletteEntry color='#E6E6E6' alpha='255' value="30"  label="30 - 331 - Beaches, dunes, sands"/>
        <paletteEntry color='#CCCCCC' alpha='255' value="31"  label="31 - 332 - Bare rocks"/>
        <paletteEntry color='#CCFFCC' alpha='255' value="32"  label="32 - 333 - Sparsely vegetated areas"/>
        <paletteEntry color='#000000' alpha='255' value="33"        label="33 - 334 - Burnt areas"/>
        <paletteEntry color='#A6E6CC' alpha='255' value="34"  label="34 - 335 - Glaciers and perpetual snow"/>
        <paletteEntry color='#A6A6FF' alpha='255' value="35"  label="35 - 411 - Inland marshes"/>
        <paletteEntry color='#4D4DFF' alpha='255' value="36"    label="36 - 412 - Peat bogs"/>
        <paletteEntry color='#CCCCFF' alpha='255' value="37"  label="37 - 421 - Salt marshes"/>
        <paletteEntry color='#E6E6FF' alpha='255' value="38"  label="38 - 422 - Salines"/>
        <paletteEntry color='#A6A6E6' alpha='255' value="39"  label="39 - 423 - Intertidal flats"/>
        <paletteEntry color='#00CCF2' alpha='255' value="40"    label="40 - 511 - Water courses"/>
        <paletteEntry color='#80F2E6' alpha='255' value="41"  label="41 - 512 - Water bodies"/>
        <paletteEntry color='#00FFA6' alpha='255' value="42"    label="42 - 521 - Coastal lagoons"/>
        <paletteEntry color='#A6FFE6' alpha='255' value="43"  label="43 - 522 - Estuaries"/>
        <paletteEntry color='#E6F2FF' alpha='255' value="48"  label="48 - 523 - Sea and ocean"/>
        <paletteEntry color='#000000' alpha='255' value="49"        label="49 - 999 - NODATA"/>
        <paletteEntry color='#FFFFFF' alpha='255' value="50"  label="50 - 990 - UNCLASSIFIED LAND SURFACE"/>
        <paletteEntry color='#E6F2FF' alpha='255' value="255"  label="255 - 995 - UNCLASSIFIED WATER BODIES"/>
      </colorPalette>
      <colorramp type="preset" name="[source]">
        <Option type="Map">
          <Option type="QString" value="250,75,60,255" name="preset_color_0"/>
          <Option type="QString" value="#fa4b3c" name="preset_color_name_0"/>
          <Option type="QString" value="preset" name="rampType"/>
        </Option>
        <prop v="250,75,60,255" k="preset_color_0"/>
        <prop v="#fa4b3c" k="preset_color_name_0"/>
        <prop v="preset" k="rampType"/>
      </colorramp>
    </rasterrenderer>
    <brightnesscontrast gamma="1" contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" grayscaleMode="0" colorizeOn="0" colorizeRed="255" saturation="0" colorizeBlue="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
