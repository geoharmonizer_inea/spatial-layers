<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>sol_log.oc_lucas.iso.10694_m_30m_s0..0cm_2000_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>Soil log organic carbon content in weight fraction [g/kg]</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#d77f3f" quantity="16" label="4" opacity="1.0"/>
                            <sld:ColorMapEntry color="#b0783a" quantity="19.2" label="6" opacity="1.0"/>
                            <sld:ColorMapEntry color="#c7a75c" quantity="22.4" label="8" opacity="1.0"/>
                            <sld:ColorMapEntry color="#e7d57a" quantity="25.6" label="12" opacity="1.0"/>
                            <sld:ColorMapEntry color="#a5ba6f" quantity="28.8" label="17" opacity="1.0"/>
                            <sld:ColorMapEntry color="#6ca363" quantity="32" label="24" opacity="1.0"/>
                            <sld:ColorMapEntry color="#3e8a59" quantity="35.2" label="32" opacity="1.0"/>
                            <sld:ColorMapEntry color="#346945" quantity="38.4" label="44" opacity="1.0"/>
                            <sld:ColorMapEntry color="#183e29" quantity="41.6" label="66" opacity="1.0"/>
                            <sld:ColorMapEntry color="#373724" quantity="44.8" label="89" opacity="1.0"/>
                            <sld:ColorMapEntry color="#050603" quantity="48" label="121" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
