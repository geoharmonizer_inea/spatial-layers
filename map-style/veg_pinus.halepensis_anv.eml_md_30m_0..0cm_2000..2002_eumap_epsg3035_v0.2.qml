<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+8" version="3.16.3-Hannover" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" fetchMode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling maxOversampling="2" zoomedOutResamplingMethod="nearestNeighbour" zoomedInResamplingMethod="nearestNeighbour" enabled="false"/>
    </provider>
    <rasterrenderer alphaBand="-1" classificationMax="50" type="singlebandpseudocolor" band="1" opacity="1" classificationMin="1" nodataColor="">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0" labelPrecision="0" maximumValue="50" classificationMode="2" minimumValue="1">
          <colorramp name="[source]" type="preset">
            <prop v="68,1,84,255" k="preset_color_0"/>
            <prop v="58,82,139,255" k="preset_color_1"/>
            <prop v="32,144,141,255" k="preset_color_2"/>
            <prop v="93,201,98,255" k="preset_color_3"/>
            <prop v="253,231,37,255" k="preset_color_4"/>
            <prop v="0" k="preset_color_name_0"/>
            <prop v="10" k="preset_color_name_1"/>
            <prop v="15" k="preset_color_name_2"/>
            <prop v="20" k="preset_color_name_3"/>
            <prop v="25" k="preset_color_name_4"/>
            <prop v="preset" k="rampType"/>
          </colorramp>
          <item color="#440154" value="1" label="1" alpha="255"/>
          <item color="#3a528b" value="13.25" label="13" alpha="255"/>
          <item color="#20908d" value="25.5" label="26" alpha="255"/>
          <item color="#5dc962" value="37.75" label="38" alpha="255"/>
          <item color="#fde725" value="50" label="50" alpha="255"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0" gamma="1"/>
    <huesaturation colorizeStrength="100" colorizeOn="0" colorizeBlue="128" colorizeGreen="128" saturation="0" grayscaleMode="0" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
