<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>sol_clay.tot.psa_lucas.iso.10694_m_30m_s0..0cm_2020_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>Soil clay content in weigth fraction [%]</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#8359a6" quantity="0" label="0" opacity="1.0"/>
                            <sld:ColorMapEntry color="#aa6fb1" quantity="4.5" label="4" opacity="1.0"/>
                            <sld:ColorMapEntry color="#aa6fb1" quantity="8.999999999999998" label="9" opacity="1.0"/>
                            <sld:ColorMapEntry color="#deaac6" quantity="13.500000000000002" label="14" opacity="1.0"/>
                            <sld:ColorMapEntry color="#e3cdd5" quantity="18" label="18" opacity="1.0"/>
                            <sld:ColorMapEntry color="#ebdbc2" quantity="22.5" label="23" opacity="1.0"/>
                            <sld:ColorMapEntry color="#f8c98a" quantity="27" label="27" opacity="1.0"/>
                            <sld:ColorMapEntry color="#f8ad58" quantity="31.499999999999996" label="31" opacity="1.0"/>
                            <sld:ColorMapEntry color="#eb8b31" quantity="36" label="36" opacity="1.0"/>
                            <sld:ColorMapEntry color="#d26716" quantity="40.5" label="41" opacity="1.0"/>
                            <sld:ColorMapEntry color="#d26716" quantity="45" label="45" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
