<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#73ffd9"/>
        <prop k="preset_color_name_0" v="64"/>
        <prop k="preset_color_1" v="#73ffe0"/>
        <prop k="preset_color_name_1" v="65"/>
        <prop k="preset_color_2" v="#73ffe8"/>
        <prop k="preset_color_name_2" v="65"/>
        <prop k="preset_color_3" v="#73fff0"/>
        <prop k="preset_color_name_3" v="66"/>
        <prop k="preset_color_4" v="#73fff7"/>
        <prop k="preset_color_name_4" v="67"/>
        <prop k="preset_color_5" v="#73ffff"/>
        <prop k="preset_color_name_5" v="67"/>
        <prop k="preset_color_6" v="#77f7ff"/>
        <prop k="preset_color_name_6" v="68"/>
        <prop k="preset_color_7" v="#7bf0ff"/>
        <prop k="preset_color_name_7" v="69"/>
        <prop k="preset_color_8" v="#7fe8ff"/>
        <prop k="preset_color_name_8" v="70"/>
        <prop k="preset_color_9" v="#7fe1ff"/>
        <prop k="preset_color_name_9" v="70"/>
        <prop k="preset_color_10" v="#7fdaff"/>
        <prop k="preset_color_name_10" v="71"/>
        <prop k="preset_color_11" v="#7fd3ff"/>
        <prop k="preset_color_name_11" v="72"/>
        <prop k="preset_color_12" v="#7fccff"/>
        <prop k="preset_color_name_12" v="72"/>
        <prop k="preset_color_13" v="#7fc5ff"/>
        <prop k="preset_color_name_13" v="73"/>
        <prop k="preset_color_14" v="#7fbeff"/>
        <prop k="preset_color_name_14" v="74"/>
        <prop k="preset_color_15" v="#7fb7ff"/>
        <prop k="preset_color_name_15" v="74"/>
        <prop k="preset_color_16" v="#7fb0ff"/>
        <prop k="preset_color_name_16" v="75"/>
        <prop k="preset_color_17" v="#7fa9ff"/>
        <prop k="preset_color_name_17" v="76"/>
        <prop k="preset_color_18" v="#7fa2ff"/>
        <prop k="preset_color_name_18" v="77"/>
        <prop k="preset_color_19" v="#7f9fff"/>
        <prop k="preset_color_name_19" v="77"/>
        <prop k="preset_color_20" v="#7f98ff"/>
        <prop k="preset_color_name_20" v="78"/>
        <prop k="preset_color_21" v="#7f95ff"/>
        <prop k="preset_color_name_21" v="79"/>
        <prop k="preset_color_22" v="#7f8dff"/>
        <prop k="preset_color_name_22" v="79"/>
        <prop k="preset_color_23" v="#7a88fa"/>
        <prop k="preset_color_name_23" v="80"/>
        <prop k="preset_color_24" v="#7583f5"/>
        <prop k="preset_color_name_24" v="81"/>
        <prop k="preset_color_25" v="#707ef0"/>
        <prop k="preset_color_name_25" v="81"/>
        <prop k="preset_color_26" v="#6b79eb"/>
        <prop k="preset_color_name_26" v="82"/>
        <prop k="preset_color_27" v="#6674e6"/>
        <prop k="preset_color_name_27" v="83"/>
        <prop k="preset_color_28" v="#616fe1"/>
        <prop k="preset_color_name_28" v="83"/>
        <prop k="preset_color_29" v="#616fdd"/>
        <prop k="preset_color_name_29" v="84"/>
        <prop k="preset_color_30" v="#616fd9"/>
        <prop k="preset_color_name_30" v="85"/>
        <prop k="preset_color_31" v="#616fd5"/>
        <prop k="preset_color_name_31" v="86"/>
        <prop k="preset_color_32" v="#616fd1"/>
        <prop k="preset_color_name_32" v="86"/>
        <prop k="preset_color_33" v="#616fcd"/>
        <prop k="preset_color_name_33" v="87"/>
        <prop k="preset_color_34" v="#616fc8"/>
        <prop k="preset_color_name_34" v="88"/>
        <prop k="preset_color_35" v="#5e6cc3"/>
        <prop k="preset_color_name_35" v="88"/>
        <prop k="preset_color_36" v="#5c6abe"/>
        <prop k="preset_color_name_36" v="89"/>
        <prop k="preset_color_37" v="#5765b9"/>
        <prop k="preset_color_name_37" v="90"/>
        <prop k="preset_color_38" v="#5260b4"/>
        <prop k="preset_color_name_38" v="90"/>
        <prop k="preset_color_39" v="#4d5baf"/>
        <prop k="preset_color_name_39" v="91"/>
        <prop k="preset_color_40" v="#4856aa"/>
        <prop k="preset_color_name_40" v="92"/>
        <prop k="preset_color_41" v="#4351a5"/>
        <prop k="preset_color_name_41" v="92"/>
        <prop k="preset_color_42" v="#3e4ca0"/>
        <prop k="preset_color_name_42" v="93"/>
        <prop k="preset_color_43" v="#39479b"/>
        <prop k="preset_color_name_43" v="94"/>
        <prop k="preset_color_44" v="#344296"/>
        <prop k="preset_color_name_44" v="95"/>
        <prop k="preset_color_45" v="#2f3d91"/>
        <prop k="preset_color_name_45" v="95"/>
        <prop k="preset_color_46" v="#2a388c"/>
        <prop k="preset_color_name_46" v="96"/>
        <prop k="preset_color_47" v="#253387"/>
        <prop k="preset_color_name_47" v="97"/>
        <prop k="preset_color_48" v="#202e82"/>
        <prop k="preset_color_name_48" v="97"/>
        <prop k="preset_color_49" v="#1b297d"/>
        <prop k="preset_color_name_49" v="98"/>
        <prop k="preset_color_50" v="#162478"/>
        <prop k="preset_color_name_50" v="99"/>
        <prop k="preset_color_51" v="#111f73"/>
        <prop k="preset_color_name_51" v="99"/>
        <prop k="preset_color_52" v="#0c1a6e"/>
        <prop k="preset_color_name_52" v="100"/>
        <prop k="preset_color_53" v="#070b69"/>
        <prop k="preset_color_name_53" v="101"/>
        <prop k="preset_color_54" v="#020664"/>
        <prop k="preset_color_name_54" v="102"/>
        <prop k="preset_color_55" v="#00015f"/>
        <prop k="preset_color_name_55" v="102"/>
        <prop k="preset_color_56" v="#00005a"/>
        <prop k="preset_color_name_56" v="103"/>
        <prop k="preset_color_57" v="#000055"/>
        <prop k="preset_color_name_57" v="104"/>
        <prop k="preset_color_58" v="#000050"/>
        <prop k="preset_color_name_58" v="104"/>
        <prop k="preset_color_59" v="#000050"/>
        <prop k="preset_color_name_59" v="105"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="64" color="#73ffd9" value="64"/>
        <item alpha="255" label="65" color="#73ffe0" value="64.6929"/>
        <item alpha="255" label="65" color="#73ffe8" value="65.3899"/>
        <item alpha="255" label="66" color="#73fff0" value="66.0828"/>
        <item alpha="255" label="67" color="#73fff7" value="66.7798"/>
        <item alpha="255" label="67" color="#73ffff" value="67.4727"/>
        <item alpha="255" label="68" color="#77f7ff" value="68.1697"/>
        <item alpha="255" label="69" color="#7bf0ff" value="68.8626"/>
        <item alpha="255" label="70" color="#7fe8ff" value="69.5596"/>
        <item alpha="255" label="70" color="#7fe1ff" value="70.2525"/>
        <item alpha="255" label="71" color="#7fdaff" value="70.9495"/>
        <item alpha="255" label="72" color="#7fd3ff" value="71.6424"/>
        <item alpha="255" label="72" color="#7fccff" value="72.3394"/>
        <item alpha="255" label="73" color="#7fc5ff" value="73.03229999999999"/>
        <item alpha="255" label="74" color="#7fbeff" value="73.7293"/>
        <item alpha="255" label="74" color="#7fb7ff" value="74.4222"/>
        <item alpha="255" label="75" color="#7fb0ff" value="75.1192"/>
        <item alpha="255" label="76" color="#7fa9ff" value="75.8121"/>
        <item alpha="255" label="77" color="#7fa2ff" value="76.5091"/>
        <item alpha="255" label="77" color="#7f9fff" value="77.202"/>
        <item alpha="255" label="78" color="#7f98ff" value="77.899"/>
        <item alpha="255" label="79" color="#7f95ff" value="78.5919"/>
        <item alpha="255" label="79" color="#7f8dff" value="79.2889"/>
        <item alpha="255" label="80" color="#7a88fa" value="79.98179999999999"/>
        <item alpha="255" label="81" color="#7583f5" value="80.6788"/>
        <item alpha="255" label="81" color="#707ef0" value="81.3717"/>
        <item alpha="255" label="82" color="#6b79eb" value="82.0687"/>
        <item alpha="255" label="83" color="#6674e6" value="82.7616"/>
        <item alpha="255" label="83" color="#616fe1" value="83.4586"/>
        <item alpha="255" label="84" color="#616fdd" value="84.1515"/>
        <item alpha="255" label="85" color="#616fd9" value="84.8485"/>
        <item alpha="255" label="86" color="#616fd5" value="85.5414"/>
        <item alpha="255" label="86" color="#616fd1" value="86.2384"/>
        <item alpha="255" label="87" color="#616fcd" value="86.9313"/>
        <item alpha="255" label="88" color="#616fc8" value="87.6283"/>
        <item alpha="255" label="88" color="#5e6cc3" value="88.3212"/>
        <item alpha="255" label="89" color="#5c6abe" value="89.01820000000001"/>
        <item alpha="255" label="90" color="#5765b9" value="89.7111"/>
        <item alpha="255" label="90" color="#5260b4" value="90.4081"/>
        <item alpha="255" label="91" color="#4d5baf" value="91.101"/>
        <item alpha="255" label="92" color="#4856aa" value="91.798"/>
        <item alpha="255" label="92" color="#4351a5" value="92.4909"/>
        <item alpha="255" label="93" color="#3e4ca0" value="93.1879"/>
        <item alpha="255" label="94" color="#39479b" value="93.8808"/>
        <item alpha="255" label="95" color="#344296" value="94.5778"/>
        <item alpha="255" label="95" color="#2f3d91" value="95.2707"/>
        <item alpha="255" label="96" color="#2a388c" value="95.9677"/>
        <item alpha="255" label="97" color="#253387" value="96.6606"/>
        <item alpha="255" label="97" color="#202e82" value="97.35759999999999"/>
        <item alpha="255" label="98" color="#1b297d" value="98.0505"/>
        <item alpha="255" label="99" color="#162478" value="98.7475"/>
        <item alpha="255" label="99" color="#111f73" value="99.4404"/>
        <item alpha="255" label="100" color="#0c1a6e" value="100.1374"/>
        <item alpha="255" label="101" color="#070b69" value="100.8303"/>
        <item alpha="255" label="102" color="#020664" value="101.5273"/>
        <item alpha="255" label="102" color="#00015f" value="102.2202"/>
        <item alpha="255" label="103" color="#00005a" value="102.91720000000001"/>
        <item alpha="255" label="104" color="#000055" value="103.61009999999999"/>
        <item alpha="255" label="104" color="#000050" value="104.30709999999999"/>
        <item alpha="255" label="105" color="#000050" value="105"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

