<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>GRA_2015_020m_eu_03035_V1_4_2</sld:Name>
            <sld:Description>Generated by SLD4raster - https://cbsuygulama.wordpress.com/sld4raster</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#00244a" quantity="1" label="OSM (IUCN Ia)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#1565c0" quantity="20" label="OSM (IUCN Ib)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#1976d2" quantity="30" label="OSM (IUCN 2)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#1e88e5" quantity="40" label="OSM (IUCN 3)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#42a5f5" quantity="50" label="OSM (IUCN 4)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#64b5f6" quantity="60" label="OSM (IUCN 5)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#90caf9" quantity="70" label="OSM (IUCN 6)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#c000e6" quantity="80" label="OSM (Others)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#d4b139" quantity="90" label="Natura2000 (A)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#b9870c" quantity="100" label="Natura2000 (B)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#9a6716" quantity="110" label="Natura2000 (C)" opacity="1.0"/>
                            <sld:ColorMapEntry color="#cf4100" quantity="120" label="Protected areas overlap" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
