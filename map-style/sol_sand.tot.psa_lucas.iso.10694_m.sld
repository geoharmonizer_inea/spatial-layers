<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>sol_sand.tot.psa_lucas.iso.10694_m_30m_s100..100cm_2020_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>Soil sand content in weight fraction [%]</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#8359a6" quantity="5" label="5" opacity="1.0"/>
                            <sld:ColorMapEntry color="#aa6fb1" quantity="14" label="14" opacity="1.0"/>
                            <sld:ColorMapEntry color="#ca8aba" quantity="23" label="23" opacity="1.0"/>
                            <sld:ColorMapEntry color="#deaac6" quantity="32" label="32" opacity="1.0"/>
                            <sld:ColorMapEntry color="#e3cdd5" quantity="41" label="41" opacity="1.0"/>
                            <sld:ColorMapEntry color="#ebdbc2" quantity="50" label="50" opacity="1.0"/>
                            <sld:ColorMapEntry color="#f8c98a" quantity="59" label="59" opacity="1.0"/>
                            <sld:ColorMapEntry color="#f8ad58" quantity="68" label="68" opacity="1.0"/>
                            <sld:ColorMapEntry color="#eb8b31" quantity="77" label="77" opacity="1.0"/>
                            <sld:ColorMapEntry color="#d26716" quantity="86" label="86" opacity="1.0"/>
                            <sld:ColorMapEntry color="#d26716" quantity="95" label="95" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
