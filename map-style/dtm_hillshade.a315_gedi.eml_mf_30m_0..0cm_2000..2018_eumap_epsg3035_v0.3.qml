<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#050505"/>
        <prop k="preset_color_name_0" v="145"/>
        <prop k="preset_color_1" v="#2e2e2e"/>
        <prop k="preset_color_name_1" v="163"/>
        <prop k="preset_color_2" v="#575757"/>
        <prop k="preset_color_name_2" v="182"/>
        <prop k="preset_color_3" v="#7f7f7f"/>
        <prop k="preset_color_name_3" v="200"/>
        <prop k="preset_color_4" v="#a8a8a8"/>
        <prop k="preset_color_name_4" v="218"/>
        <prop k="preset_color_5" v="#d1d1d1"/>
        <prop k="preset_color_name_5" v="237"/>
        <prop k="preset_color_6" v="#fafafa"/>
        <prop k="preset_color_name_6" v="255"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="145" color="#050505" value="145"/>
        <item alpha="255" label="163" color="#2e2e2e" value="163.33333333333334"/>
        <item alpha="255" label="182" color="#575757" value="181.66666666666666"/>
        <item alpha="255" label="200" color="#7f7f7f" value="200"/>
        <item alpha="255" label="218" color="#a8a8a8" value="218.33333333333331"/>
        <item alpha="255" label="237" color="#d1d1d1" value="236.66666666666666"/>
        <item alpha="255" label="255" color="#fafafa" value="255"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

