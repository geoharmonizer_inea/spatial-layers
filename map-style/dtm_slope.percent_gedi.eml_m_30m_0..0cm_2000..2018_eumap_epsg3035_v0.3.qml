<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="1e+08" version="3.10.4-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="2" opacity="1" classificationMax="19">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
      <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
      <colorramp type="preset" name="[source]">
      	<prop k="preset_color_0" v="#fef0d9"/>
        <prop k="preset_color_name_0" v="0"/>
        <prop k="preset_color_1" v="#fdd49e"/>
        <prop k="preset_color_name_1" v="217"/>
        <prop k="preset_color_2" v="#fdbb84"/>
        <prop k="preset_color_name_2" v="433"/>
        <prop k="preset_color_3" v="#fc8d59"/>
        <prop k="preset_color_name_3" v="650"/>
        <prop k="preset_color_4" v="#e34a33"/>
        <prop k="preset_color_name_4" v="867"/>
        <prop k="preset_color_5" v="#b30000"/>
        <prop k="preset_color_name_5" v="1083"/>
        <prop k="preset_color_6" v="#830000"/>
        <prop k="preset_color_name_6" v="1300"/>
        
        <prop k="rampType" v="preset"/>
      </colorramp>
      <item alpha="255" label="0" color="#fef0d9" value="0"/>
        <item alpha="255" label="217" color="#fdd49e" value="216.70999999999998"/>
        <item alpha="255" label="433" color="#fdbb84" value="433.28999999999996"/>
        <item alpha="255" label="650" color="#fc8d59" value="650"/>
        <item alpha="255" label="867" color="#e34a33" value="866.7099999999999"/>
        <item alpha="255" label="1083" color="#b30000" value="1083.29"/>
        <item alpha="255" label="1300" color="#830000" value="1300"/>
         
       </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeStrength="100" colorizeBlue="128" saturation="0" grayscaleMode="0"/>
    <rasterresampler maxOversampling="3" zoomedOutResampler="bilinear"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>

