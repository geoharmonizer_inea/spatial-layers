<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3</sld:Name>
            <sld:Description>Elevation</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#00bfbf" label="-150" opacity="1.0" quantity="-150"/>
                            <sld:ColorMapEntry color="#00ff00" label="4680" opacity="1.0" quantity="4680"/>
                            <sld:ColorMapEntry color="#ffff00" label="9510" opacity="1.0" quantity="9510"/>
                            <sld:ColorMapEntry color="#ff7f00" label="14340" opacity="1.0" quantity="14340"/>
                            <sld:ColorMapEntry color="#bf7f3f" label="19170" opacity="1.0" quantity="19170"/>
                            <sld:ColorMapEntry color="#141414" label="24000" opacity="1.0" quantity="24000"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
