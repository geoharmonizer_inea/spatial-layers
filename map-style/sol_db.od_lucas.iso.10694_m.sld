<?xml version="1.0" ?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" version="1.0.0">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>sol_db.od_lucas.iso.10694_m_30m_s100..100cm_2020_eumap_epsg3035_v0.2</sld:Name>
            <sld:Description>Soil bulk density in 10 kg / m-cubic [10 kg/m3]</sld:Description>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#1b0a22" quantity="10" label="100" opacity="1.0"/>
                            <sld:ColorMapEntry color="#1e0c26" quantity="21.57142857142857" label="220" opacity="1.0"/>
                            <sld:ColorMapEntry color="#210d2a" quantity="33.14285714285714" label="330" opacity="1.0"/>
                            <sld:ColorMapEntry color="#240e2f" quantity="44.714285714285715" label="450" opacity="1.0"/>
                            <sld:ColorMapEntry color="#271033" quantity="56.285714285714285" label="560" opacity="1.0"/>
                            <sld:ColorMapEntry color="#2a1137" quantity="67.85714285714286" label="680" opacity="1.0"/>
                            <sld:ColorMapEntry color="#2d133b" quantity="79.42857142857143" label="790" opacity="1.0"/>
                            <sld:ColorMapEntry color="#301440" quantity="91" label="910" opacity="1.0"/>
                            <sld:ColorMapEntry color="#3a2d7a" quantity="102.57142857142857" label="1030" opacity="1.0"/>
                            <sld:ColorMapEntry color="#3f9aff" quantity="114.14285714285714" label="1140" opacity="1.0"/>
                            <sld:ColorMapEntry color="#26eda6" quantity="125.71428571428571" label="1260" opacity="1.0"/>
                            <sld:ColorMapEntry color="#b0f936" quantity="137.28571428571428" label="1370" opacity="1.0"/>
                            <sld:ColorMapEntry color="#fcb236" quantity="148.85714285714286" label="1490" opacity="1.0"/>
                            <sld:ColorMapEntry color="#e14109" quantity="160.42857142857142" label="1600" opacity="1.0"/>
                            <sld:ColorMapEntry color="#7a0403" quantity="172" label="1720" opacity="1.0"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
