<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0" version="3.20.2-Odense" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal fetchMode="0" mode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option name="WMSBackgroundLayer" type="bool" value="false"/>
      <Option name="WMSPublishDataSourceUrl" type="bool" value="false"/>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="identify/format" type="QString" value="Value"/>
    </Option>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" enabled="false" maxOversampling="2" zoomedOutResamplingMethod="nearestNeighbour"/>
    </provider>
    <rasterrenderer nodataColor="" classificationMax="95" type="singlebandpseudocolor" classificationMin="5" alphaBand="-1" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader labelPrecision="0" maximumValue="95" clip="0" colorRampType="INTERPOLATED" classificationMode="1" minimumValue="5">
          <colorramp name="[source]" type="cpt-city">
            <Option type="Map">
              <Option name="inverted" type="QString" value="0"/>
              <Option name="rampType" type="QString" value="cpt-city"/>
              <Option name="schemeName" type="QString" value="km/purple-orange-d"/>
              <Option name="variantName" type="QString" value="10"/>
            </Option>
            <prop v="0" k="inverted"/>
            <prop v="cpt-city" k="rampType"/>
            <prop v="km/purple-orange-d" k="schemeName"/>
            <prop v="10" k="variantName"/>
          </colorramp>
          <item color="#8359a6" label="5" alpha="255" value="5"/>
          <item color="#aa6fb1" label="14" alpha="255" value="14"/>
          <item color="#ca8aba" label="23" alpha="255" value="23"/>
          <item color="#deaac6" label="32" alpha="255" value="32"/>
          <item color="#e3cdd5" label="41" alpha="255" value="41"/>
          <item color="#ebdbc2" label="50" alpha="255" value="50"/>
          <item color="#f8c98a" label="59" alpha="255" value="59"/>
          <item color="#f8ad58" label="68" alpha="255" value="68"/>
          <item color="#eb8b31" label="77" alpha="255" value="77"/>
          <item color="#d26716" label="86" alpha="255" value="86"/>
          <item color="#d26716" label="95" alpha="255" value="95"/>
          <rampLegendSettings direction="0" minimumLabel="" maximumLabel="" orientation="2" prefix="" useContinuousLegend="1" suffix="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option name="decimal_separator" type="QChar" value=""/>
                <Option name="decimals" type="int" value="6"/>
                <Option name="rounding_type" type="int" value="0"/>
                <Option name="show_plus" type="bool" value="false"/>
                <Option name="show_thousand_separator" type="bool" value="true"/>
                <Option name="show_trailing_zeros" type="bool" value="false"/>
                <Option name="thousand_separator" type="QChar" value=""/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast gamma="1" brightness="0" contrast="0"/>
    <huesaturation colorizeOn="0" colorizeRed="255" grayscaleMode="0" colorizeStrength="100" saturation="0" colorizeBlue="128" colorizeGreen="128"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
