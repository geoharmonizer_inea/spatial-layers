{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Image](actinia_logo.png)\n",
    "\n",
    "## User defined processing\n",
    "\n",
    "### The actinia process chain\n",
    "\n",
    "Actinia provides the **process chain** approach to specify import,\n",
    "processing and export of geodata using the actinia GRASS GIS processing\n",
    "system. The process chain must be formulated in JSON. The processing is\n",
    "always performed in an ephemeral database. The computational environment\n",
    "is based on locations in the persistent database. If required, the\n",
    "ephemeral database can be moved into the persistent user database, so\n",
    "that the computational results can be used in further processing steps\n",
    "or visualized using the actinia rendering REST calls.\n",
    "\n",
    "The ephemeral database will be removed after computation. However, all\n",
    "raster and vector data that was generated during the processing can be\n",
    "exported using gdal/ogr specific datatypes and stored in an object\n",
    "storage, outside the actinia environment. Within a process chain we have\n",
    "read only access to all raster maps of the persistent database location\n",
    "that is used as computational environment.\n",
    "\n",
    "A process chain is a list of [GRASS GIS modules](https://grass.osgeo.org/grass-stable/manuals/index.html) that will be executed in\n",
    "serial, based on the order of the list. GRASS GIS modules are specified\n",
    "as [process definitions](https://actinia.mundialis.de/api_docs/#/definitions/GrassModule) that include the name of the command, the\n",
    "[inputs](https://actinia.mundialis.de/api_docs/#/definitions/InputParameter) and [outputs](https://actinia.mundialis.de/api_docs/#/definitions/OutputParameter), including import and export definitions as\n",
    "well as the module flags.\n",
    "\n",
    "This notebook demonstrates the actinia REST service access using the Python\n",
    "library **requests**.\n",
    "\n",
    "The following example demonstrates how to create actinia process chains to downscale ERA5 data to a higher resolution with the help of CHELSA data and how to cacluate relative humidity from air temperature and dewpoint temperature. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### actinia API documentation\n",
    "\n",
    "* [Stable actinia API v3 docs](https://redocly.github.io/redoc/?url=https://actinia.mundialis.de/api/v3/swagger.json)\n",
    "* [Development actinia API v3 docs](https://redocly.github.io/redoc/?url=https://actinia-dev.mundialis.de/api/v3/swagger.json)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### Requirements\n",
    "\n",
    "#### Software & Modules\n",
    "\n",
    "This example assumes your are comfortable with the [Python](https://python.org) programming language. Familiarity with basic REST API concepts and usage is also assumed.\n",
    "\n",
    "Python modules used in this tutorial are:\n",
    "* [requests](http://docs.python-requests.org/)\n",
    "* [json](https://docs.python.org/3/library/json.html)\n",
    "\n",
    "\n",
    "#### ACTINIA API user and password\n",
    "\n",
    "This demo requires credentials for authentication set below in **Preparation** as a variable. Another actinia instance might require different credentials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Helper Modules and Functions\n",
    "Before interacting with the actinia server using Python, we will import required packages an set up a helper function to print formatted JSON using json.\n",
    "\n",
    "***Note:*** *You may need to install two helpful browser plugins called **RESTman** and **JSON Formatter** that format JSON and makes it easier to read:*\n",
    "\n",
    "* [RESTman extension](https://chrome.google.com/webstore/detail/restman/ihgpcfpkpmdcghlnaofdmjkoemnlijdi)\n",
    "* [JSON Formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# first, let's import the required packages.\n",
    "\n",
    "from pprint import pprint\n",
    "import sys\n",
    "import json\n",
    "import time\n",
    "\n",
    "import requests\n",
    "from requests.auth import HTTPBasicAuth\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "scrolled": true
   },
   "source": [
    "To simplify our life in terms of server communication we store the credentials and REST server URL in  variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# variables to set the actinia host, version, and user\n",
    "\n",
    "actinia_baseurl = \"https://actinia.mundialis.de\"\n",
    "actinia_version = \"v3\"\n",
    "actinia_url = actinia_baseurl + \"/api/\" + actinia_version\n",
    "actinia_auth = HTTPBasicAuth('demouser', 'gu3st!pa55w0rd')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# helper function to print formatted JSON using the json module\n",
    "def print_as_json(data):\n",
    "    print(json.dumps(data, indent=2))\n",
    "\n",
    "# helper function to verify a request\n",
    "def verify_request(request, success_code=200):\n",
    "    if request.status_code != success_code:\n",
    "        print(\"ERROR: actinia processing failed with status code %d!\" % request.status_code)\n",
    "        print(\"See errors below:\")\n",
    "        print_as_json(request.json())\n",
    "        request_url = request.json()[\"urls\"][\"status\"]\n",
    "        requests.delete(url=request_url, auth=actinia_auth)\n",
    "        raise Exception(\"The resource <%s> has been terminated.\" % request_url)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set up a helper function to create list items "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# helper function to create a process chain item\n",
    "def create_actinia_pc_item(id, module,\n",
    "                           inputs=None, outputs=None, flags=None, stdin=None, stdout=None,\n",
    "                           overwrite=False, superquiet=False, verbose=False, interface_description=False):\n",
    "    \"\"\"\n",
    "    Creates a list item for an actinia process chain\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    id: str\n",
    "        unique id for this item\n",
    "    module: str\n",
    "        some valid GRASS or actinia module\n",
    "    inputs: list or dict\n",
    "        list of input parameters with values in the form\n",
    "        [{\"param\": key1, \"value\": value1}, {\"param\": key2, \"value\": value2}, ...]\n",
    "        shorter alternative as dict\n",
    "        {\"key1\": value1, \"key2\": value2, ...}\n",
    "    outputs: list or dict\n",
    "        list of output parameters with values in the form\n",
    "        [{\"param\": key1, \"value\": value1}, {\"param\": key2, \"value\": value2}, ...]\n",
    "        shorter alternative as dict\n",
    "        {\"key1\": value1, \"key2\": value2, ...}\n",
    "    flags: str\n",
    "        optional flags for the module\n",
    "    stdin: dict\n",
    "        options to read stdin\n",
    "    stdout: dict\n",
    "        options to write to stdout\n",
    "        must be of the form\n",
    "        {\"id\": value1, \"format\": value2, \"delimiter\": value3}\n",
    "    overwrite: bool\n",
    "        optional, set to True to allow overwriting existing data\n",
    "    superquiet: bool\n",
    "        optional, set to True to suppress all messages but errors\n",
    "    verbose: bool\n",
    "        optional, set to True to allow verbose messages\n",
    "    interface_description: bool\n",
    "        optional, set to True to create an interface_description\n",
    "    \"\"\"\n",
    "    pc_item = {\"id\": id, \"module\": module}\n",
    "    if inputs:\n",
    "        if isinstance(inputs, list):\n",
    "            pc_item[\"inputs\"] = inputs\n",
    "        elif isinstance(inputs, dict):\n",
    "            tmplist = []\n",
    "            for k, v in inputs.items():\n",
    "                tmplist.append({\"param\": k, \"value\": v})\n",
    "            pc_item[\"inputs\"] = tmplist\n",
    "    if outputs:\n",
    "        if isinstance(outputs, list):\n",
    "            pc_item[\"inputs\"] = inputs\n",
    "        elif isinstance(outputs, dict):\n",
    "            tmplist = []\n",
    "            for k, v in outputs.items():\n",
    "                tmplist.append({\"param\": k, \"value\": v})\n",
    "            pc_item[\"outputs\"] = tmplist\n",
    "    if flags:\n",
    "        pc_item[\"flags\"] = flags\n",
    "    if stdin:\n",
    "        pc_item[\"stdin\"] = stdin\n",
    "    if stdout:\n",
    "        pc_item[\"stdout\"] = stdout\n",
    "    if overwrite is True:\n",
    "        pc_item[\"overwrite\"] = True\n",
    "    if superquiet is True:\n",
    "        pc_item[\"superquiet\"] = True\n",
    "    if verbose is True:\n",
    "        pc_item[\"verbose\"] = True\n",
    "    if interface_description is True:\n",
    "        pc_item[\"interface_description\"] = True\n",
    "\n",
    "    return pc_item\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Examples\n",
    "\n",
    "### Temperature\n",
    "\n",
    "We use the spatial detail of CHELSA climatologies and the temporal detail of ERA5 Land to create a temperature timeseries with the spatial resolution of CHELSA.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we set some variables and create an empty process chain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# name of a ERA5 temperature raster (air temperature 2m above ground)\n",
    "# here for 2000-01-01\n",
    "era5_temp = \"era5_land_daily_t2m_20000101_avg\"\n",
    "\n",
    "# name of a CHELSA temperature raster\n",
    "# here for january\n",
    "chelsa_temp = \"chelsa_tmean_01_global\"\n",
    "\n",
    "# create an empty process chain\n",
    "process_chain = {\"version\": 1, \"list\": []}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add items to the process chain list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set the computational region to align with ERA5\n",
    "list_id = 1\n",
    "\n",
    "# short form, accepted by create_actinia_pc_item\n",
    "inputs = {\"n\": \"82:03N\",\n",
    "          \"s\": \"18:27N\",\n",
    "          \"w\": \"56:03W\",\n",
    "          \"e\": \"61:09E\",\n",
    "          \"res\": \"00:06:00\"}\n",
    "\n",
    "stdout = {\"id\": \"region\", \"format\": \"kv\", \"delimiter\": \"=\"}\n",
    "flags = \"g\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.region\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# spatially aggregate CHELSA to ERA5 resolution\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"input\": chelsa_temp,\n",
    "          \"method\": \"median\"}\n",
    "outputs = {\"output\": \"chelsa_6min\"}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.resamp.stats\",\n",
    "                                 inputs=inputs,\n",
    "                                 outputs=outputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# difference ERA5 - CHELSA\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "expr = \"era5_diff = %s - chelsa_6min\" % era5_temp\n",
    "inputs = {\"expression\": expr}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.mapcalc\",\n",
    "                                 inputs=inputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set region to 30 sec, aligned to the CHELSA grid geometry\n",
    "list_id += 1\n",
    "\n",
    "# short form, accepted by create_actinia_pc_item\n",
    "inputs = {\"n\": \"72:26:59.5N\",\n",
    "          \"s\": \"24:08:59.5N\",\n",
    "          \"w\": \"56:03:00.5W\",\n",
    "          \"e\": \"61:08:59.5E\",\n",
    "          \"res\": \"00:00:30\"}\n",
    "\n",
    "stdout = {\"id\": \"region\", \"format\": \"kv\", \"delimiter\": \"=\"}\n",
    "flags = \"g\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.region\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# interpolate differences to higher (CHELSA) resolution\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"input\": \"era5_diff\",\n",
    "          \"filter\": \"gauss,box\",\n",
    "          \"radius\": \"0.15,0.25\"}\n",
    "outputs = {\"output\": \"era5_diff_30sec\"}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.resamp.filter\",\n",
    "                                 inputs=inputs,\n",
    "                                 outputs=outputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add the interpolated difference to the reference map\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "expr = \"%s_30sec = round(era5_diff_30sec + %s)\" % (era5_temp, chelsa_temp)\n",
    "inputs = {\"expression\": expr}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.mapcalc\",\n",
    "                                 inputs=inputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove temporary raster maps\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"type\": \"raster\",\n",
    "          \"name\": \"chelsa_6min,era5_diff,era5_diff_30sec\"}\n",
    "flags = \"f\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.remove\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save actinia process chain to file\n",
    "f = open(\"actinia_pc_era5_t2m.json\", mode='w')\n",
    "print(json.dumps(process_chain, indent=2), file=f)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Precipitation\n",
    "\n",
    "The procedure is similar to the one above for temperature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "# name of a ERA5 precipitation raster\n",
    "# here for 2000-01-01\n",
    "era5_prectot = \"era5_land_daily_prectot_20000101_sum\"\n",
    "\n",
    "# name of a CHELSA precipitation raster\n",
    "# here for january\n",
    "chelsa_prectot = \"chelsa_prec_01_global\"\n",
    "\n",
    "# create an empty process chain\n",
    "process_chain = {\"version\": 1, \"list\": []}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add items to the process chain list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set the computational region to align with ERA5\n",
    "list_id = 1\n",
    "\n",
    "# short form, accepted by create_actinia_pc_item\n",
    "inputs = {\"n\": \"82:03N\",\n",
    "          \"s\": \"18:27N\",\n",
    "          \"w\": \"56:03W\",\n",
    "          \"e\": \"61:09E\",\n",
    "          \"res\": \"00:06:00\"}\n",
    "\n",
    "stdout = {\"id\": \"region\", \"format\": \"kv\", \"delimiter\": \"=\"}\n",
    "flags = \"g\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.region\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "# spatially aggregate CHELSA to ERA5 resolution\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"input\": chelsa_prectot,\n",
    "          \"method\": \"median\"}\n",
    "outputs = {\"output\": \"chelsa_6min\"}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.resamp.stats\",\n",
    "                                 inputs=inputs,\n",
    "                                 outputs=outputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "# proportion ERA5 / CHELSA\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "expr = \"era5_prop = float(%s) - chelsa_6min\" % era5_prectot\n",
    "inputs = {\"expression\": expr}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.mapcalc\",\n",
    "                                 inputs=inputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set region to 30 sec, aligned to the CHELSA grid geometry\n",
    "list_id += 1\n",
    "\n",
    "# short form, accepted by create_actinia_pc_item\n",
    "inputs = {\"n\": \"72:26:59.5N\",\n",
    "          \"s\": \"24:08:59.5N\",\n",
    "          \"w\": \"56:03:00.5W\",\n",
    "          \"e\": \"61:08:59.5E\",\n",
    "          \"res\": \"00:00:30\"}\n",
    "\n",
    "stdout = {\"id\": \"region\", \"format\": \"kv\", \"delimiter\": \"=\"}\n",
    "flags = \"g\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.region\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "# interpolate proportions to higher (CHELSA) resolution\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"input\": \"era5_prop\",\n",
    "          \"filter\": \"gauss,box\",\n",
    "          \"radius\": \"0.15,0.25\"}\n",
    "outputs = {\"output\": \"era5_prop_30sec\"}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.resamp.filter\",\n",
    "                                 inputs=inputs,\n",
    "                                 outputs=outputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "# multiply the interpolated porportions with the reference map\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "expr = \"%s_30sec = round(era5_prop_30sec * %s)\" % (era5_prectot, chelsa_prectot)\n",
    "inputs = {\"expression\": expr}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.mapcalc\",\n",
    "                                 inputs=inputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove temporary raster maps\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "inputs = {\"type\": \"raster\",\n",
    "          \"name\": \"chelsa_6min,era5_prop,era5_prop_30sec\"}\n",
    "flags = \"f\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.remove\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save actinia process chain to file\n",
    "f = open(\"actinia_pc_era5_prectot.json\", mode='w')\n",
    "print(json.dumps(process_chain, indent=2), file=f)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calculating relative humidity from air temperature and dewpoint temperature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# name of a raster with air temperature 2m above ground\n",
    "# here for 2000-01-01\n",
    "era5_t2m = \"era5_land_daily_t2m_20000101_avg_30sec\"\n",
    "\n",
    "# name of a raster with dewpoint temperature 2m above ground\n",
    "# here for 2000-01-01\n",
    "era5_tdp2m = \"era5_land_daily_tdp2m_20000101_avg_30sec\"\n",
    "\n",
    "# resultant raster with relative humidity\n",
    "era5_rh2m = \"era5_land_daily_rh2m_20000101_avg_30sec\"\n",
    "\n",
    "# create an empty process chain\n",
    "process_chain = {\"version\": 1, \"list\": []}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set region to 30 sec, aligned to the CHELSA grid geometry\n",
    "list_id += 1\n",
    "\n",
    "# short form, accepted by create_actinia_pc_item\n",
    "inputs = {\"n\": \"72:26:59.5N\",\n",
    "          \"s\": \"24:08:59.5N\",\n",
    "          \"w\": \"56:03:00.5W\",\n",
    "          \"e\": \"61:08:59.5E\",\n",
    "          \"res\": \"00:00:30\"}\n",
    "\n",
    "stdout = {\"id\": \"region\", \"format\": \"kv\", \"delimiter\": \"=\"}\n",
    "flags = \"g\"\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"g.region\",\n",
    "                                 inputs=inputs,\n",
    "                                 flags=flags)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate relative humidity\n",
    "# units will be percent * 10, can be stored with datatype 16bit integer\n",
    "list_id += 1\n",
    "\n",
    "# short form\n",
    "expr = \"%(rh2m)s = round(1000 * exp(17.502 * 0.1 * %(tdp2m)s / (240.97 + 0.1 * %(tdp2m)s)) / exp(17.502 * 0.1 * %(t2m)s / (240.97 + 0.1 * %(t2m)s)))\" % {\"rh2m\": era5_rh2m, \"tdp2m\": era5_tdp2m, \"t2m\": era5_t2m}\n",
    "inputs = {\"expression\": expr}\n",
    "\n",
    "pc_item = create_actinia_pc_item(id=list_id,\n",
    "                                 module=\"r.mapcalc\",\n",
    "                                 inputs=inputs)\n",
    "process_chain[\"list\"].append(pc_item)\n",
    "\n",
    "#print_as_json(process_chain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save actinia process chain to file\n",
    "f = open(\"actinia_pc_era5_rh2m.json\", mode='w')\n",
    "print(json.dumps(process_chain, indent=2), file=f)\n",
    "f.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
