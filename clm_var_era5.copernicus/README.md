# Example actinia process chains

Actinia is an open source REST API for scalable, distributed, high performance processing of geographical data that uses mainly GRASS GIS for computational tasks (DOI: https://doi.org/10.5281/zenodo.5879231).

These process chains show some parts of the ERA5 data resampling.

 * air temperature 2m above ground: actinia_pc_era5_t2m.json
 * daily precipitation: actinia_pc_era5_prectot.json
 * relative humidity: actinia_pc_era5_rh2m.json
 
Requirements: [actinia](https://github.com/mundialis/actinia_core) instance
