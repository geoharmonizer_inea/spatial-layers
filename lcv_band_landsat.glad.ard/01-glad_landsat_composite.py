import sys
import os
from pathlib import Path
from urllib3.exceptions import MaxRetryError

from eumap.raster import save_rasters
from eumap.misc import ttprint
from eumap import parallel
import pandas as pd
import geopandas as gpd

def job(worker, worker_args, n_jobs = -1, joblib_args = {}):
  from joblib import Parallel, delayed

  joblib_args['n_jobs'] = n_jobs

  for worker_result in Parallel(**joblib_args)(delayed(worker)(*args) for args in worker_args):
    yield worker_result

tiles = gpd.read_file('./glad_landsat_tiles_eumap.gpkg')
tile_ids = list(tiles['TILE'])

user = '<GLAD_LANDSAT_USER>'
password = '<GLAD_LANDSAT_PASSWORD>'
p_list = [25, 50, 75]
start_year = 2021
end_year = 2005

output_dir = 'mangrove'

args = []
for tile_id in tile_ids:
  for year in range(start_year, (end_year-1), -1):
    args.append((tile_id, f'{year-1}-22', f'{year}-5', p_list, output_dir, user, password))
    args.append((tile_id, f'{year}-6', f'{year}-11', p_list, output_dir, user, password))
    args.append((tile_id, f'{year}-12', f'{year}-16', p_list, output_dir, user, password))
    args.append((tile_id, f'{year}-17', f'{year}-21', p_list, output_dir, user, password))

def run(tile, start, end, p, output_dir, user, password):
  from eumap.datasets.eo import GLADLandsat
  from minio import Minio

  host = "<MINIO_S3_HOST>"
  access_key = "<MINIO_S3_ACCESS_KEY>"
  access_secret = "<MINIO_S3_ACCESS_SECRET>"
  bucket_name = 'tmp'

  client = Minio(host, access_key, access_secret, secure=False)
  file_check = f'{output_dir}/{start}/B3_P50/{tile}.tif'

  try:
    result = client.stat_object("tmp", file_check)
    exists = True
  except:
    exists = False

  if exists:
    ttprint(f"Skipping tile {tile}, already processed ({file_check})")
  else:
    keep_going = True

    while keep_going:
      try:
        landsat = GLADLandsat(user, password, filter_additional_qa=False)
        _,_, output_files = landsat.percentile_agg(tile, start, end, p, output_dir=output_dir)

        for output_fn_file in output_files:
          object_bucket = f'{bucket_name}'
          ttprint(f'Copying {output_fn_file} to http://{host}/{object_bucket}/{output_fn_file}')
          client.fput_object(object_bucket, str(output_fn_file), output_fn_file)
          os.remove(output_fn_file)

        keep_going = False

      except MaxRetryError as ex:
        print(f'{tile}', ex)
        continue
      except Exception:
        print(f'ERROR in tile {tile} {start} {end}')
        return False

  return True

for output_files in job(run, args, n_jobs=3, joblib_args={'backend': 'multiprocessing'}):
  continue