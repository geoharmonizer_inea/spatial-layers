import requests
import sys
import os
import rasterio
import psutil
import gc
import shutil
import traceback

from minio import Minio

from eumap import parallel
from eumap.misc import ttprint
from eumap import gapfiller
from eumap.raster import read_rasters, save_rasters, write_new_raster
from rasterio.windows import from_bounds
import numpy as np
import pandas as pd

import glob
from pathlib import Path

from concurrent.futures import as_completed, wait, FIRST_COMPLETED, ProcessPoolExecutor
import psutil
import time
import gc

def _mem_usage():
  mem = psutil.virtual_memory()
  return (mem.used / mem.total)

def _run_step(i, step, mem_usage_limit, mem_check_interval, mem_check, verbose, *args):
  while (_mem_usage() > mem_usage_limit and mem_check):
    if verbose:
      ttprint(f'Memory usage in {_mem_usage():.2f}%, stopping process for step_{i}')
    time.sleep(mem_check_interval)
    gc.collect()
  
  return step(*args)

def _raster_files(tile, base_url, start_year, end_year, percs, bands):
  raster_files_dict = {}
  for year in range(start_year, (end_year)+1):
    for perc in percs:
      for band in bands:
        key = f'{perc}_{band}'

        if not _processed(band, perc, tile):
          if key not in raster_files_dict:
            raster_files_dict[key] = []

          raster_files_dict[key].append(Path(f'{base_url}/{year-1}-22/{band}_{perc}/{tile}.tif'))
          raster_files_dict[key].append(Path(f'{base_url}/{year}-6/{band}_{perc}/{tile}.tif'))
          raster_files_dict[key].append(Path(f'{base_url}/{year}-12/{band}_{perc}/{tile}.tif'))
          raster_files_dict[key].append(Path(f'{base_url}/{year}-17/{band}_{perc}/{tile}.tif'))
        else:
          ttprint(f"SKIPING {tile} {key} because it was already processed.")

  return raster_files_dict

def read(raster_files, img_size, out_dir):
  data, _ = read_rasters(raster_files=raster_files, expected_img_size=img_size, n_jobs=20, verbose=True)
  return data, raster_files, out_dir

def process(data, raster_files, out_dir):
  try:
    tfsl = gapfiller.time_first_space_later(
      data = data,
      time_strategy = gapfiller.TMWM,
      time_args = { 'time_win_size': 7, 'season_size': 4, 'cpu_max_workers':-1 },
      space_strategy = gapfiller.InPainting,
      space_args = { 'space_win': 5, 'data_mask': land_mask.astype('int') },
      space_flag_val = 100
    )

    tfsl.run()

    del data
    gc.collect()

    return (tfsl.gapfilled_data, tfsl.gapfilled_data_flag, raster_files, out_dir)
  except:
    traceback.print_exc()
    ttprint(f"ERROR: {raster_files[0]}")
    return (None, None, None, None)

def save(gapfilled_data, gapfilled_data_flag, raster_files, out_dir):
  
  if gapfilled_data is not None:

    base_raster = _base_raster(raster_files)
    gapfilled_files, flag_files = [], []
    
    for file in raster_files:
      file_split = str(file).split('/')
      date = file_split[-3]
      band = file_split[-2]
      
      parent_dir = out_dir.joinpath(date).joinpath(band)

      gapfilled_fn = parent_dir.joinpath(file.name)
      flag_fn = parent_dir.joinpath(file.name.replace('.tif', '_flag.tif'))
      
      gapfilled_files.append(gapfilled_fn)
      flag_files.append(flag_fn)
    
    output_files = save_rasters(base_raster, gapfilled_files, gapfilled_data, 
       n_jobs=15, verbose=True) + \
            save_rasters(base_raster, flag_files, gapfilled_data_flag, 
       n_jobs=15, verbose=True)

    del gapfilled_files
    del gapfilled_data_flag
    gc.collect()

    return output_files

  else:
    return []

def _processed(band, perc, tile):
  url = f'http://<MINIO_S3_HOST>/tmp/eumap_gapfilled/2000-22/{band}_{perc}/{tile}.tif'
  r = requests.head(url)
  return (r.status_code == 200)

def _base_raster(raster_files):
  for url in raster_files:
    url = 'http://' + str(str(url).split(':/')[1])
    r = requests.head(url)
    if r.status_code == 200:
      return url

  return None

def run(tile, base_url, start_year, end_year, percs, bands, land_mask_fn,
  out_dir, tile_size, minio_client):

  land_mask_ds = rasterio.open(land_mask_fn)

  raster_files_dict = _raster_files(tile, base_url, start_year, end_year, percs, bands)
  keys = list(raster_files_dict.keys())
  
  if len(keys) > 0:

    base_raster = _base_raster(raster_files_dict[keys[0]])

    if base_raster is None:
      ttprint(f'None raster files for {tile}')
      return False

    base_ds = rasterio.open(base_raster)
    bounds = base_ds.bounds
    land_mask_win = from_bounds(base_ds.bounds.left, base_ds.bounds.bottom, base_ds.bounds.right, \
      base_ds.bounds.top, land_mask_ds.transform).round_lengths(op='ceil', pixel_precision=0)

    land_mask, _ = read_rasters(raster_files=[land_mask_fn], spatial_win=land_mask_win, n_jobs=1)
    land_mask = (land_mask != 0)[:,:,0]

    output_files = parallel.TaskSequencer(
     params=iter([ (raster_files_dict[key], tile_size, out_dir) for key in keys ]),
     steps=[ (read, 1, True), (process, 5, False), (save, 1, False) ]
    ).run()

    output_files = sum(output_files, [])

    bucket_name = 'tmp'
    out_pref = f'eumap_gapfilled'

    for output_fn_file in output_files:
      object_name = '/'.join(str(output_fn_file).split('/')[-3:]) 
      object_bucket = f'{bucket_name}'
      ttprint(f'Copying {output_fn_file} to http://{host}/{object_bucket}/{out_pref}/{object_name}')
      minio_client.fput_object(object_bucket, f'{out_pref}/{object_name}', output_fn_file)
      os.remove(output_fn_file)

start_tile=int(sys.argv[1])
end_tile=int(sys.argv[2])
server_name=sys.argv[3]

tiles = gpd.read_file('./glad_landsat_tiles_eumap.gpkg')
tile_ids = list(tiles['TILE'])
tiles = sorted(tiles)

base_url = "http://<MINIO_S3_HOST>/tmp/eumap"

start_year, end_year = 2000, 2021
percs = ['P25', 'P50', 'P75']
bands = [ f'B{i}' for i in range(1,8)]
tile_size = (4004,4004)

out_dir = Path(f'/mnt/{server_name}/eumap/gapfilling/')
land_mask_fn = 'http://<MINIO_S3_HOST>/eumap/lcv/lcv_landcover.12_pflugmacher2019_c_1m_s0..0m_2014..2016_eumap_epsg3035_v0.1.tif'

host = "<MINIO_S3_HOST>"
access_key = "<MINIO_S3_ACCESS_KEY>"
access_secret = "<MINIO_S3_ACCESS_SECRET>"
minio_client = Minio(host, access_key, access_secret, secure=False)

for tile in tiles[start_tile:end_tile]:
  run(tile, base_url, start_year, end_year, percs, bands, land_mask_fn, out_dir, tile_size)